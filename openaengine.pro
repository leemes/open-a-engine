#-------------------------------------------------
#
# Project created by QtCreator 2012-09-08T22:43:02
#
#-------------------------------------------------

QT += core gui

TARGET = openaengine
TEMPLATE = lib
CONFIG += staticlib

DEFINES += OAE_LIBRARY

#DEFINES += OAE_NAMESPACE=oae


SOURCES += oae/openaengine.cpp \
    oae/world.cpp \
    oae/island.cpp \
    oae/town.cpp \
    oae/player.cpp \
    oae/game.cpp \
    oae/error.cpp \
    oae/building.cpp \
    oae/libraries/abstractlibrary.cpp \
    oae/libraries/buildinglibrary.cpp \
    oae/prototypes/buildingprototype.cpp \
    oae/libraries/roadlibrary.cpp \
    oae/prototypes/roadprototype.cpp \
    oae/internal/scheduler.cpp \
    oae/internal/scheduled.cpp \
    oae/internal/datastructures/sparsebitarray2d.cpp \
    oae/libraries/goodlibrary.cpp \
    oae/prototypes/goodprototype.cpp \
    oae/manager.cpp \
    oae/command.cpp \
    oae/goods.cpp \
    oae/internal/commands/command_addgoodsisland.cpp \
    oae/internal/commands/abstractcommand.cpp \
    oae/commandinterface.cpp \
    oae/internal/commands/command_createbuilding.cpp \
    oae/internal/commands/command_createroad.cpp \
    oae/internal/commands/command_deleteroad.cpp \
    oae/libraries/transportationlibrary.cpp \
    oae/prototypes/transportationprototype.cpp \
    oae/routingparameters.cpp \
    oae/internal/routing/routingconditionfunctions.cpp \
    oae/transportation.cpp \
    oae/internal/routing/routecalculation.cpp \
    oae/path.cpp \
    oae/internal/commands/command_debug.cpp \
    oae/objectbase.cpp

HEADERS += oae/openaengine.h \
    oae/world.h \
    oae/island.h \
    oae/town.h \
    oae/player.h \
    oae/game.h \
    oae/global.h \
    oae/error.h \
    oae/building.h \
    oae/namespace.h \
    oae/libraries/abstractlibrary.h \
    oae/libraries/buildinglibrary.h \
    oae/prototypes/buildingprototype.h \
    oae/libraries/roadlibrary.h \
    oae/prototypes/roadprototype.h \
    oae/internal/scheduler.h \
    oae/internal/scheduled.h \
    oae/limits.h \
    oae/internal/datastructures/sparsebitarray2d.h \
    oae/libraries/goodlibrary.h \
    oae/prototypes/goodprototype.h \
    oae/manager.h \
    oae/command.h \
    oae/goods.h \
    oae/internal/commands/command_addgoodsisland.h \
    oae/internal/commands/abstractcommand.h \
    oae/commandinterface.h \
    oae/internal/commands/command_createbuilding.h \
    oae/internal/commands/command_createroad.h \
    oae/internal/commands/command_deleteroad.h \
    oae/libraries/transportationlibrary.h \
    oae/prototypes/transportationprototype.h \
    oae/routingparameters.h \
    oae/internal/routing/routingconditionfunctions.h \
    oae/internal/hashfunctions.h \
    oae/transportation.h \
    oae/internal/routing/routecalculation.h \
    oae/path.h \
    oae/internal/euclideanlength.h \
    oae/internal/commands/command_debug.h \
    oae/objectbase.h \
    oae/objectbase_p.h
    
unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}

