#ifndef GOODS_H
#define GOODS_H

#include "global.h"
#include <QMap>
#include <QMetaType>
#include "namespace.h"

typedef QMap<GoodID,int> GoodList;

OAE_BEGIN_NAMESPACE

class LocalGoods : public QMap<GoodID,int>
{
public:
    LocalGoods operator+(const LocalGoods & other) const;
};

class GlobalGoods : public QMap<GoodID,int>
{
};

class Goods
{
public:
    Goods(const QMap<GoodID,int> &goods = QMap<GoodID,int>());

    const GlobalGoods &globalGoods() const;
    const LocalGoods &localGoods() const;

    int operator[](GoodID good) const;
    int &operator[](GoodID good);

    // These methods apply a multiplication / division of all goods in this collection (rounded down to integers)
    Goods operator*(double factor) const;
    Goods &operator*=(double factor);
    Goods operator/(double divisor) const;
    Goods &operator/=(double divisor);

    // The same than operator/=, but the result is rounded up to the next integer
    Goods &divideRoundedUp(double divisor);

    QVariantMap toVariantMap() const;

private:
    GlobalGoods m_global;
    LocalGoods m_local;
};

OAE_END_NAMESPACE

Q_DECLARE_METATYPE(GlobalGoods)
Q_DECLARE_METATYPE(LocalGoods)

#endif // GOODS_H
