#ifndef OBJECTBASE_H
#define OBJECTBASE_H

#include <QObject>
#include <QSharedPointer>

class ObjectBasePrivate;

class ObjectBase : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(ObjectBase)
    Q_DECLARE_PRIVATE(ObjectBase)

protected:
    QSharedPointer<ObjectBasePrivate> d_ptr;

    ObjectBase(QObject *parent);
    virtual ~ObjectBase() = 0;

signals:
    
public slots:


};

#endif // OBJECTBASE_H
