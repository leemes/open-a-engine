#ifndef PLAYER_H
#define PLAYER_H

#include "global.h"
#include <QObject>
#include <QMap>
#include "namespace.h"

OAE_BEGIN_NAMESPACE

class Game;
class Town;
class Island;

class Player : public QObject
{
    Q_OBJECT

public:
    explicit Player(Game *game, PlayerID id);

public slots:
    PlayerID id() const;

    Town *newTown(Island *island, QString name);

    const QMap<GoodID,int> & currentGlobalGoods() const;
    int currentGlobalGood(GoodID good) const;
    int currentCoins() const;
    bool checkGlobalGoods(QMap<GoodID,int> goods) const;

private:
libprivate:
    void addGlobalGoods(QMap<GoodID,int> goods);
    bool takeGlobalGoods(QMap<GoodID,int> goods);

    void addCoins(int amount);
    void takeCoins(int amount);

private:
    PlayerID m_id;

    QList<Town*> m_towns;
    QMap<GoodID,int> m_globalGoods;
};

OAE_END_NAMESPACE

#endif // PLAYER_H
