#include "error.h"
#include "openaengine.h"


Error::Error()
{
}

void Error::report(Game *game, Error::Type type, QVariantList args)
{
    Error e;
    e.m_game = game;
    e.m_type = type;
    e.m_args = args;

    if(OpenAEngine::staticInstance)
        OpenAEngine::staticInstance->registerNewError(e);
    else
        qFatal("Error occured when no OpenAEngine instance has been created.");
}

void Error::report(Game *game, Error::Type type, QVariant arg1)
{
    report(game, type, QVariantList() << arg1);
}

void Error::report(Game *game, Error::Type type, QVariant arg1, QVariant arg2)
{
    report(game, type, QVariantList() << arg1 << arg2);
}

void Error::report(Game *game, Error::Type type, QVariant arg1, QVariant arg2, QVariant arg3)
{
    report(game, type, QVariantList() << arg1 << arg2 << arg3);
}

Game *Error::game() const
{
    return m_game;
}

Error::Type Error::type() const
{
    return m_type;
}

QVariantList Error::arguments() const
{
    return m_args;
}

QVariant Error::argumentAt(int i) const
{
    return m_args.at(i);
}
