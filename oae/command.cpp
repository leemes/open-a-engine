#include "command.h"
#include "game.h"
#include "world.h"
#include "manager.h"
#include "internal/commands/abstractcommand.h"


Command::Command(Game *game, AbstractCommand *command) :
    QObject(0),
    m_game(game),
    m_command(command)
{
    Q_ASSERT(m_game);
    Q_ASSERT(m_command);
}

Command::~Command()
{
    delete m_command;
}

void Command::execute()
{
    Error error = m_command->check();
    if(error == NoError)
        m_command->execute();
    emit done(error);
}

Game *Command::game() const
{
    return m_game;
}

Time Command::time() const
{
    return m_time;
}

Command::Error Command::check() const
{
    return m_command->check();
}
