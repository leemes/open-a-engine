#ifndef BUILDINGLIBRARY_H
#define BUILDINGLIBRARY_H

#include "../global.h"
#include "abstractlibrary.h"
#include "../prototypes/buildingprototype.h"

OAE_BEGIN_NAMESPACE

class BuildingLibrary : public AbstractLibrary
{
public:
    static void initialize();
    static void dumpContents();

    static BuildingPrototype *prototype(uint protoID);
    static BuildingPrototype *prototype(QString name);

private:
    BuildingLibrary(){}

    static BuildingPrototype::Type readBuildingType(QVariant type);
    static QMap<int,int> readGrantBuildingLicenses(QVariant licenses, int influenceRadius);

    static bool initialized;
    static QVector<BuildingPrototype> prototypes;
};

OAE_END_NAMESPACE

#endif // BUILDINGLIBRARY_H
