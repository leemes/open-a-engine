#ifndef GOODLIBRARY_H
#define GOODLIBRARY_H

#include "../global.h"
#include "../prototypes/goodprototype.h"
#include "abstractlibrary.h"

OAE_BEGIN_NAMESPACE

class GoodLibrary : public AbstractLibrary
{
public:
    static void initialize();
    static void dumpContents();

    static GoodPrototype *prototype(GoodID good);
    static GoodPrototype *prototype(QString name);

private:
    GoodLibrary(){}

    static bool initialized;
    static QVector<GoodPrototype> prototypes;
};

OAE_END_NAMESPACE

#endif // GOODLIBRARY_H
