#include "roadlibrary.h"
#include <QJsonDocument>
#include <QVariant>
#include <QDebug>
#include "../error.h"
#include "../goods.h"

bool RoadLibrary::initialized = false;
QVector<RoadPrototype> RoadLibrary::prototypes;


void RoadLibrary::initialize()
{
    if(initialized)
        return;

    QString filePath = rulesBaseDir().filePath("roads.json");
    qDebug("init roads (from %s)...", qPrintable(filePath));

    QFile file(filePath);
    file.open(QFile::ReadOnly);
    QVariantMap data = QJsonDocument::fromJson(file.readAll()).toVariant().toMap();
    file.close();

    foreach(QString classStr, data.keys())
    {
        RoadPrototype::Class roadClass = RoadPrototype::ClassInvalid;
        classStr = classStr.toLower();
        if(classStr == "path")
            roadClass = RoadPrototype::ClassPath;
        else if(classStr == "place")
            roadClass = RoadPrototype::ClassPlace;
        else
            qFatal("Unknown road class: %s", qPrintable(classStr));

        QVariantList roads = data[classStr].toList();
        foreach(QVariant road, roads)
        {
            QVariantMap properties = road.toMap();

            //check for name:
            if(!properties.contains("name"))
                qFatal("No name given for a road. Name is mandatory.");

            //create prototype
            RoadPrototype proto;
            proto.name = properties.value("name").toString();
            proto.roadClass = roadClass;

            //other properties:
            proto.constructionCosts = readGoods(properties.value("cost"));
            proto.constructionCostsDivisor = properties.value("cost divisor", 1.0f).toFloat();
            proto.speedFactor = properties.value("speed factor", 1.0).toFloat();
            proto.requireLicenses = readBuildingLicenses(properties.value("require license", "any"));

            //icon:
            QDir iconsRoadsDir(iconsBaseDir().filePath("roads"));
            QDir iconsClassDir(iconsRoadsDir.filePath(classStr));
            QImage icon(iconsClassDir.filePath(proto.name + ".png"));
            proto.icon = icon;

            prototypes << proto;
        }
    }

    initialized = true;
}

void RoadLibrary::dumpContents()
{
    initialize();
    foreach(RoadPrototype p, prototypes)
    {
        qDebug() << " " << p.id << p.name << p.constructionCosts.globalGoods() << p.constructionCosts.localGoods() << p.speedFactor << p.icon.size();
        if(p.requireLicenses == ~0)
            qDebug() << "    requires licenses" << "<ANY>";
        else
            qDebug() << "    requires licenses" << qPrintable(QString::number(p.requireLicenses, 2).rightJustified(licenseList.count(), '0'));
    }
}

RoadPrototype *RoadLibrary::prototype(uint protoID)
{
    initialize();
    if(protoID > (uint)prototypes.count())
        return 0;
    RoadPrototype *proto = &prototypes[protoID - 1];
    Q_ASSERT(proto->id == protoID);
    return proto;
}

RoadPrototype *RoadLibrary::prototype(QString name)
{
    initialize();
    for(int i = 0; i < prototypes.count(); ++i)
        if(prototypes[i].name.toLower() == name.toLower())
            return &prototypes[i];
    return 0;
}
