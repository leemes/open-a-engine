#ifndef ABSTRACTLIBRARY_H
#define ABSTRACTLIBRARY_H

#include "../global.h"
#include "../namespace.h"
#include <QDir>

class QSize;
class QVariant;
class QString;
class QJsonDocument;
template<class K,class T> class QMap;

OAE_BEGIN_NAMESPACE

class Goods;

class AbstractLibrary
{
public:
    AbstractLibrary() {}
    virtual ~AbstractLibrary() = 0;

    static void setRulesBaseDir(QDir rulesBaseDir);
    static void setIconsBaseDir(QDir iconsBaseDir);

protected:
    static inline QDir rulesBaseDir() { return m_rulesBaseDir; }
    static inline QDir iconsBaseDir() { return m_iconsBaseDir; }

    static QJsonDocument readJsonDocument(QString filePath);
    static QSize readSize(QVariant size);
    static Goods readGoods(QVariant goods);
    static GoodID readGood(QVariant good);
    static int readBuildingLicenses(QVariant licenses);
    static int readBuildingLicense(QString license);

protected:
    static QMap<QString,int> licenseList;

private:
    static QDir m_rulesBaseDir;
    static QDir m_iconsBaseDir;
};

OAE_END_NAMESPACE

#endif // ABSTRACTLIBRARY_H
