#include "buildinglibrary.h"
#include "goodlibrary.h"
#include <QJsonDocument>
#include <QVariant>
#include <QDebug>
#include <numeric>
#include "../error.h"
#include "../limits.h"
#include "../namespace.h"
#include "../goods.h"
#include "transportationlibrary.h"


bool BuildingLibrary::initialized = false;
QVector<BuildingPrototype> BuildingLibrary::prototypes;


void BuildingLibrary::initialize()
{
    if(initialized)
        return;

    QString filePath = rulesBaseDir().filePath("buildings.json");
    qDebug("init buildings (from %s)...", qPrintable(filePath));


    QVariantMap data = readJsonDocument(filePath).toVariant().toMap();

    foreach(QString classStr, data.keys())
    {
        BuildingPrototype::Class buildingClass = BuildingPrototype::ClassInvalid;
        classStr = classStr.toLower();
        if(classStr == "trade")
            buildingClass = BuildingPrototype::ClassTrade;
        else if(classStr == "military")
            buildingClass = BuildingPrototype::ClassMilitary;
        else if(classStr == "production")
            buildingClass = BuildingPrototype::ClassProduction;
        else if(classStr == "decorative")
            buildingClass = BuildingPrototype::ClassDecorative;
        else if(classStr == "need")
            buildingClass = BuildingPrototype::ClassNeed;
        else if(classStr == "residence")
            buildingClass = BuildingPrototype::ClassResidence;
        else
            qFatal("Unknown building class: %s", qPrintable(classStr));

        QVariantList buildings = data[classStr].toList();
        foreach(QVariant building, buildings)
        {
            QVariantMap properties = building.toMap();

            //check for name:
            if(!properties.contains("name"))
                Error::report(0, Error::RulesParseError, "No name given for a building. Name is mandatory.");

            /*
            //based on:
            QString basedOn = properties.value("based on").toString();
            if(!basedOn.isEmpty() && !prototype(basedOn))
                Error::report(0, Error::RulesParseError, QString("Building \"%1\" is based on \"%2\", but this is "
                                                                 "unknown (parent building has to appear before child "
                                                                 "building)").arg(properties.value("name").toString(),
                                                                                  basedOn));

            //create prototype based on parent prototype:
            BuildingPrototype *protoBase = prototype(basedOn); //inherit properties
            BuildingPrototype proto = protoBase ? BuildingPrototype(*protoBase) : BuildingPrototype();
            */
            BuildingPrototype proto;

            //main properties:
            proto.name = properties.value("name").toString();
            proto.buildingClass = buildingClass;

            //other properties:
            proto.buildingType = readBuildingType(properties.value("type"));
            proto.size = readSize(properties.value("size", QVariantList() << 1 << 1));
            if(properties.value("cost").isValid())
                proto.constructionCosts = readGoods(properties.value("cost"));
            else
                proto.canBeBuiltDirectly = false;
            proto.constructionCostsDivisor = properties.value("cost divisor", 1.0f).toFloat();
            proto.influenceRadius = properties.value("radius", 8).toFloat();
            proto.maintenanceActive = properties.value("maintenance", 0).toInt();
            proto.maintenanceInactive = properties.value("maintenance inactive", 0).toInt();
            proto.storageCapacity = properties.value("storage capacity", 0).toInt();
            proto.strongboxSlots = properties.value("strongbox slots", 0).toInt();
            proto.tradeSlots = properties.value("trade slots", 0).toInt();
            proto.productionGood = readGood(properties.value("produces"));
            proto.productionTime = timeFromSeconds(properties.value("production time", 0).toFloat());
            proto.productionCapacity = properties.value("production capacity", 0).toInt();

            //building licenses:
            proto.requireLicenseOverlap = properties.contains("require building license overlap");
            if(proto.requireLicenseOverlap && properties.contains("require building license"))
                Error::report(0, Error::RulesParseError, "Only one of the properties \"require building license\" and "
                              "\"require building license overlap\" allowed on the same building.", proto.name);
            if(proto.requireLicenseOverlap)
                proto.requireLicenses = readBuildingLicenses(properties.value("require building license overlap"));
            else
                proto.requireLicenses = readBuildingLicenses(properties.value("require building license", "any"));
            proto.grantLicenses = readGrantBuildingLicenses(properties.value("grant building license"), proto.influenceRadius);

            //transportation units:
            for(uint transportationProtoID = 1; transportationProtoID <= TransportationLibrary::maxPrototypeID(); ++transportationProtoID)
            {
                TransportationPrototype *transportationProto = TransportationLibrary::prototype(transportationProtoID);
                if(properties.contains(transportationProto->propertyName))
                    proto.transportationUnits[transportationProtoID] = properties.value(transportationProto->propertyName).toInt();
            }

            //icon:
            QDir iconsBuildingsDir(iconsBaseDir().filePath("buildings"));
            QDir iconsClassDir(iconsBuildingsDir.filePath(classStr));
            QImage icon(iconsClassDir.filePath(proto.name + ".png"));
            proto.icon = icon;

            prototypes << proto;
        }
    }

    initialized = true;
}

void BuildingLibrary::dumpContents()
{
    initialize();
    foreach(const BuildingPrototype &p, prototypes)
    {
        qDebug() << " " << p.id << p.name << p.size << p.constructionCosts.globalGoods() << p.constructionCosts.localGoods() << p.icon.size();
        if(p.requireLicenses == ~0)
            qDebug() << "    requires licenses" << "<ANY>"
                     << (p.requireLicenseOverlap ? "(overlap)" : "");
        else
            qDebug() << "    requires licenses" << qPrintable(QString::number(p.requireLicenses, 2).rightJustified(licenseList.count(), '0'))
                     << (p.requireLicenseOverlap ? "(overlap)" : "");
        if(!p.grantLicenses.empty()) {
            QList<int> grantLicenses = p.grantLicenses.keys();
            int bitfield = std::accumulate(grantLicenses.begin(), grantLicenses.end(), 0);
            qDebug() << "    granting licenses" << qPrintable(QString::number(bitfield, 2).rightJustified(licenseList.count(), '0'));
        }
        foreach(BuildingPrototype::Upgrade up, p.upgrades)
            qDebug() << "    upgradeable to" << up.targetPrototype->name << "for" << up.upgradeCosts.globalGoods() << up.upgradeCosts.localGoods();
    }
}

BuildingPrototype *BuildingLibrary::prototype(uint protoID)
{
    initialize();
    if(protoID > (uint)prototypes.count())
        return 0;
    BuildingPrototype *proto = &prototypes[protoID - 1];
    Q_ASSERT(proto->id == protoID);
    return proto;
}

BuildingPrototype *BuildingLibrary::prototype(QString name)
{
    initialize();
    for(int i = 0; i < prototypes.count(); ++i)
        if(prototypes[i].name.toLower() == name.toLower())
            return &prototypes[i];
    return 0;
}

BuildingPrototype::Type BuildingLibrary::readBuildingType(QVariant type)
{
    if(type.isValid()) {
        QString t = type.toString().toLower();
        if(t == "land")
            return BuildingPrototype::TypeLand;
        if(t == "coast")
            return BuildingPrototype::TypeCoast;
        if(t == "water")
            return BuildingPrototype::TypeWater;
        if(t == "river")
            return BuildingPrototype::TypeRiver;
        else
            Error::report(0, Error::RulesParseError, "Unknown building type: " + t);
    }
    return (BuildingPrototype::Type)0;
}

QMap<int, int> BuildingLibrary::readGrantBuildingLicenses(QVariant licenses, int influenceRadius)
{
    QMap<int, int> result;
    if(licenses.isValid()) {
        if(licenses.type() == QVariant::Map)
        {
            QVariantMap licensesMap = licenses.toMap();
            foreachkv(QString license, QVariant value, licensesMap)
            {
                int licenseFlag = readBuildingLicense(license);
                int radius;
                if(value.type() == QVariant::Int || value.type() == QVariant::Double) // Qt's QJson parses integers as doubles...
                    radius = value.toInt();
                else if(value.type() == QVariant::String && value.toString().toLower() == "radius")
                    radius = influenceRadius;
                else
                    Error::report(0, Error::RulesParseError, "Values in the object of the property \"grant building license\" have to be integers or the string \"radius\".");
                result[licenseFlag] = radius;
            }
        }
        else
            Error::report(0, Error::RulesParseError, "The property \"grant building license\" has to be an object.");
    }
    return result;
}
