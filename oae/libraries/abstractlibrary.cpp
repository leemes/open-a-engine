#include "abstractlibrary.h"
#include "goodlibrary.h"
#include "../error.h"
#include "../goods.h"
#include "../limits.h"
#include <QSize>
#include <QVariant>
#include <QDebug>
#include <QJsonDocument>

QDir AbstractLibrary::m_rulesBaseDir;
QDir AbstractLibrary::m_iconsBaseDir;
QMap<QString,int> AbstractLibrary::licenseList;


void AbstractLibrary::setRulesBaseDir(QDir rulesBaseDir)
{
    m_rulesBaseDir = rulesBaseDir;
}

void AbstractLibrary::setIconsBaseDir(QDir iconsBaseDir)
{
    m_iconsBaseDir = iconsBaseDir;
}

QJsonDocument AbstractLibrary::readJsonDocument(QString filePath)
{
    QJsonParseError jsonParseError;

    QFile file(filePath);
    file.open(QFile::ReadOnly);
    QJsonDocument json = QJsonDocument::fromJson(file.readAll(), &jsonParseError);
    file.close();

    if(jsonParseError.error)
        Error::report(0, Error::RulesParseError, "Json parse error in file " + filePath +
                      " at offset " + QString::number(jsonParseError.offset), jsonParseError.errorString());

    return json;
}

QSize AbstractLibrary::readSize(QVariant size)
{
    if(size.toList().count() != 2)
        Error::report(0, Error::RulesParseError, "Size property has to be an array with exactly two integer entries");
    return QSize(size.toList()[0].toInt(), size.toList()[1].toInt());
}

Goods AbstractLibrary::readGoods(QVariant goods)
{
    Goods result;
    if(goods.isValid()) {
        QVariantMap costsMap = goods.toMap();
        foreach(QString good, costsMap.keys())
        {
            GoodPrototype *goodProto = GoodLibrary::prototype(good);
            if(!goodProto)
                Error::report(0, Error::RulesParseError, "No such good defined", good);
            result[goodProto->id] = costsMap[good].toInt();
        }
    }
    return result;
}

GoodID AbstractLibrary::readGood(QVariant good)
{
    if(good.isValid()) {
        GoodPrototype *goodProto = GoodLibrary::prototype(good.toString());
        if(!goodProto)
            Error::report(0, Error::RulesParseError, "No such good defined", good);
        return goodProto->id;
    } else {
        return 0;
    }
}

int AbstractLibrary::readBuildingLicenses(QVariant licenses)
{
    if(licenses.isValid()) {
        if(licenses.type() == QVariant::String)
            return readBuildingLicense(licenses.toString());
        else if(licenses.type() == QVariant::List)
        {
            int result = 0;
            foreach(QVariant entry, licenses.toList())
                result |= readBuildingLicense(entry.toString());
            return result;
        }
        else
            Error::report(0, Error::RulesParseError, "Building license properties have to be arrays or strings.");
    }
    return 0;
}

int AbstractLibrary::readBuildingLicense(QString license)
{
    license = license.toLower();
    if(license == "any")
        return ~0;
    else if(licenseList.contains(license))
        return licenseList[license];
    else
    {
        if(licenseList.count() == OAE_MAX_BUILDING_LICENSES)
            Error::report(0, Error::RulesParseError, "Too many different building licenses defined. Only up to %d "
                          "licenses supported in this build of OAE.", OAE_MAX_BUILDING_LICENSES);

        int licenseFlag = 1 << licenseList.count();
        licenseList[license] = licenseFlag;
        return licenseFlag;
    }
}
