#ifndef TRANSPORTATIONLIBRARY_H
#define TRANSPORTATIONLIBRARY_H

#include "../global.h"
#include "../prototypes/transportationprototype.h"
#include "abstractlibrary.h"

OAE_BEGIN_NAMESPACE

class TransportationLibrary : public AbstractLibrary
{
public:
    static void initialize();
    static void dumpContents();

    static uint maxPrototypeID();

    static TransportationPrototype *prototype(uint protoID);
    static TransportationPrototype *prototype(QString name);

private:
    TransportationLibrary(){}

    static TransportationPrototype::RoutingConditionFunc readRoutingCondition(QVariant routingCondition);

    static bool initialized;
    static QVector<TransportationPrototype> prototypes;
};

OAE_END_NAMESPACE

#endif // TRANSPORTATIONLIBRARY_H
