#ifndef ROADLIBRARY_H
#define ROADLIBRARY_H

#include "../global.h"
#include "../prototypes/roadprototype.h"
#include "abstractlibrary.h"

OAE_BEGIN_NAMESPACE

class RoadLibrary : public AbstractLibrary
{
public:
    static void initialize();
    static void dumpContents();

    static RoadPrototype *prototype(uint protoID);
    static RoadPrototype *prototype(QString name);

private:
    RoadLibrary(){}

    static bool initialized;
    static QVector<RoadPrototype> prototypes;
};

OAE_END_NAMESPACE

#endif // ROADLIBRARY_H
