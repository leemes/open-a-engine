#include "transportationlibrary.h"
#include <QJsonDocument>
#include <QVariant>
#include <QDebug>
#include "../error.h"
#include "../goods.h"
#include "../internal/routing/routingconditionfunctions.h"


bool TransportationLibrary::initialized = false;
QVector<TransportationPrototype> TransportationLibrary::prototypes;


void TransportationLibrary::initialize()
{
    if(initialized)
        return;

    QString filePath = rulesBaseDir().filePath("transportation.json");
    qDebug("init transportations (from %s)...", qPrintable(filePath));

    QFile file(filePath);
    file.open(QFile::ReadOnly);
    QVariantList data = QJsonDocument::fromJson(file.readAll()).toVariant().toList();
    file.close();

    foreach(QVariant transportation, data)
    {
        QVariantMap properties = transportation.toMap();

        //check for name:
        if(!properties.contains("name"))
            qFatal("No name given for a transportation unit. Name is mandatory.");

        //create prototype
        TransportationPrototype proto;
        proto.name = properties.value("name").toString();

        //other properties:
        proto.propertyName = properties.value("property name", proto.name).toString();
        proto.routingCondition = readRoutingCondition(properties.value("routing condition", "road orthogonal"));
        proto.baseSpeed = properties.value("base speed", 1.0).toFloat() * msPerTimeUnit / 1000.0; // speed is given in 1/s, we want it in 1/ticks
        proto.capacity = properties.value("capacity", 1).toInt();

        //delays:
        QVariantMap delays = properties.value("delays").toMap();
        qDebug() << delays;
        proto.delays.startHome = timeFromSeconds(delays.value("home start", 0.0).toFloat());
        proto.delays.stopClient = timeFromSeconds(delays.value("client stop", 0.0).toFloat());
        proto.delays.startClient = timeFromSeconds(delays.value("client start", 0.0).toFloat());
        proto.delays.stopHome = timeFromSeconds(delays.value("home stop", 0.0).toFloat());

        prototypes << proto;
    }

    initialized = true;
}

void TransportationLibrary::dumpContents()
{
    initialize();
    foreach(TransportationPrototype p, prototypes)
    {
        qDebug() << " " << p.id << p.name << "capacity" << p.capacity << "speed" << p.baseSpeed
                 << "delays" << p.delays.startHome << p.delays.stopClient << p.delays.startClient << p.delays.stopHome;
    }
}

uint TransportationLibrary::maxPrototypeID()
{
    return TransportationPrototype::nextID - 1;
}

TransportationPrototype *TransportationLibrary::prototype(uint protoID)
{
    initialize();
    if(protoID > (uint)prototypes.count())
        return 0;
    TransportationPrototype *proto = &prototypes[protoID - 1];
    Q_ASSERT(proto->id == protoID);
    return proto;
}

TransportationPrototype *TransportationLibrary::prototype(QString name)
{
    initialize();
    for(int i = 0; i < prototypes.count(); ++i)
        if(prototypes[i].name.toLower() == name.toLower())
            return &prototypes[i];
    return 0;
}

TransportationPrototype::RoutingConditionFunc TransportationLibrary::readRoutingCondition(QVariant routingCondition)
{
    QString funcName = routingCondition.toString();
    return getRoutingConditionFunctionForName(funcName);
}
