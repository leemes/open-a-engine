#include "goodlibrary.h"
#include <QJsonDocument>
#include <QVariant>
#include <QDebug>
#include "../error.h"
#include "../namespace.h"


bool GoodLibrary::initialized = false;
QVector<GoodPrototype> GoodLibrary::prototypes;


void GoodLibrary::initialize()
{
    if(initialized)
        return;

    QString filePath = rulesBaseDir().filePath("goods.json");
    qDebug("init goods (from %s)...", qPrintable(filePath));

    QFile file(filePath);
    file.open(QFile::ReadOnly);
    QVariantMap data = QJsonDocument::fromJson(file.readAll()).toVariant().toMap();
    file.close();

    foreach(QString classStr, data.keys())
    {
        GoodPrototype::Class goodClass = GoodPrototype::ClassInvalid;
        classStr = classStr.toLower();
        if(classStr == "global")
            goodClass = GoodPrototype::ClassGlobal;
        else if(classStr == "local")
            goodClass = GoodPrototype::ClassLocal;
        else
            Error::report(0, Error::RulesParseError, "Unknown good class (Currently allowed: \"global\" (goods of a "
                    "player) and \"local\" (goods stored in a town / in a transportation unit))", classStr);

        if(goodClass == GoodPrototype::ClassLocal)
            GoodPrototype::firstLocalGoodID = GoodPrototype::nextID;

        QVariantList goods = data[classStr].toList();
        foreach(QVariant good, goods)
        {
            QVariantMap properties;
            if(good.type() == QVariant::Map)
                properties = good.toMap();
            else
                properties["name"] = good.toString();

            //check for name:
            if(!properties.contains("name"))
                Error::report(0, Error::RulesParseError, "No name given for a good. Name is mandatory.");

            //create prototype
            GoodPrototype proto;
            proto.name = properties.value("name").toString();
            proto.goodClass = goodClass;

            //special case: coins
            if(proto.name == "coins")
                GoodPrototype::coinsID = proto.id;

            //other properties:
            if(goodClass == GoodPrototype::ClassGlobal)
                proto.limit = properties.value("limit", 0).toInt();
            else if(properties.contains("limit"))
                Error::report(0, Error::RulesParseError, "Only global goods can have limits. Local goods are limited by the storage capacity of the corresponding town.");

            //icon:
            QDir iconsGoodsDir(iconsBaseDir().filePath("goods"));
            QDir iconsClassDir(iconsGoodsDir.filePath(classStr));
            QImage icon(iconsClassDir.filePath(proto.name + ".png"));
            proto.icon = icon;

            prototypes << proto;
        }
    }

    if(GoodPrototype::coinsID == 0)
        Error::report(0, Error::RulesParseError, "No good \"coins\" defined. This is a mandatory global good.");

    initialized = true;
}

void GoodLibrary::dumpContents()
{
    initialize();
    foreach(GoodPrototype p, prototypes)
    {
        qDebug() << " " << p.id << (p.goodClass == GoodPrototype::ClassGlobal ? "global" : "local")
                    << p.name << (p.limit ? qPrintable(QString("(limit: %1)").arg(p.limit)) : "");
    }
}

GoodPrototype *GoodLibrary::prototype(GoodID good)
{
    initialize();
    if(good > (uint)prototypes.count())
        return 0;
    GoodPrototype *proto = &prototypes[good - 1];
    Q_ASSERT(proto->id == good);
    return proto;
}

GoodPrototype *GoodLibrary::prototype(QString name)
{
    initialize();
    for(int i = 0; i < prototypes.count(); ++i)
        if(prototypes[i].name.toLower() == name.toLower())
            return &prototypes[i];
    return 0;
}
