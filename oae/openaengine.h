#ifndef OPENAENGINE_H
#define OPENAENGINE_H

#include "global.h"
#include "error.h"
#include "namespace.h"
#include <QObject>
#include <QDir>

OAE_BEGIN_NAMESPACE

class Game;

class OpenAEngine : public QObject
{
    Q_OBJECT
    friend class Error;

public:
    OpenAEngine(QDir resourcesBaseDir, QObject *parent = 0);

public slots:
    void initialize();

    Game *newGame(QSize size);
    Game *newGame(int width, int height);

    QList<Game*> games() const;

    QString goodName(GoodID goodID) const;

    void dumpObjects() const;
    void dumpLibraries() const;

signals:
    void error(Error);

private:
    void registerMetaTypes();
    void registerNewError(Error);

private:
    static OpenAEngine *staticInstance;

    QDir m_resBaseDir;
    QList<Game*> m_games;
};

OAE_END_NAMESPACE

#endif // OPENAENGINE_H
