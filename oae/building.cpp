#include "building.h"
#include "town.h"
#include "island.h"
#include "world.h"
#include "game.h"
#include <QRect>
#include <QMap>
#include <QDebug>
#include <cmath>
#include "prototypes/buildingprototype.h"
#include "libraries/transportationlibrary.h"
#include "libraries/goodlibrary.h" // debugging only!
#include "internal/hashfunctions.h"
#include "transportation.h"
#include "internal/routing/routecalculation.h"


Building::Building(Island *island, Town *town, BuildingPrototype *prototype, QPoint position, Orientation orientation) :
    QObject(island),
    Scheduled(island->game()),
    m_town(town),
    m_proto(prototype),
    m_pos(position),
    m_orientation(orientation)
{
    // initialize class-specific data
    switch(m_proto->buildingClass)
    {
    case BuildingPrototype::ClassProduction:
        for(int i = 0; i < Limits::maxBuildingProductionInputs; ++i)
            m_cs.production.inputFills[i] = 0;
        m_cs.production.outputFill = 0;
        m_cs.production.outputFillReservedOutgoing = 0;
        m_cs.production.productionStarted = -1;
        break;
    default:
        break;
    }

    // instantiate transportation units
    foreachkv(uint transportationProtoID, int count, m_proto->transportationUnits)
    {
        TransportationPrototype *proto = TransportationLibrary::prototype(transportationProtoID);
        for(int i = 0; i < count; ++i)
            transportationUnits << new Transportation(this, proto);
    }
}

Island *Building::island() const
{
    Q_ASSERT(qobject_cast<Island*>(parent()));
    return (Island*)parent();
}

Town *Building::town() const
{
    return m_town;
}

Player *Building::player() const
{
    return m_town->player();
}

Game *Building::game() const
{
    return island()->game();
}

QPoint Building::position() const
{
    return m_pos;
}

Orientation Building::orientation() const
{
    return m_orientation;
}

QRect Building::boundaries() const
{
    return QRect(m_pos, orientedSize(m_proto->size, m_orientation));
}

QRect Building::globalBoundaries() const
{
    return boundaries().translated(island()->boundaries().topLeft());
}

QList<QPoint> Building::coveredFields() const
{
    return m_proto->coveredFields(m_pos, m_orientation);
}

int Building::coveredFieldsCount() const
{
    return m_proto->coveredFieldsCount();
}

QList<QPoint> Building::possibleStreetConnections() const
{
    return m_proto->possibleStreetConnections(m_pos, m_orientation, island());
}

QString Building::typeName() const
{
    return m_proto->name;
}

bool Building::isTradeBuilding() const
{
    return m_proto->buildingClass == BuildingPrototype::ClassTrade;
}

bool Building::isProductionBuilding() const
{
    return m_proto->buildingClass == BuildingPrototype::ClassProduction;
}

QImage Building::icon() const
{
    return m_proto->icon;
}

int Building::storageCapacity() const
{
    return m_proto->storageCapacity;
}

float Building::influenceRadius() const
{
    return m_proto->influenceRadius;
}

QList<QPoint> Building::influenceArea() const
{
    return m_proto->areaForRadius(m_pos, m_orientation, influenceRadius(), island());
}

QList<QPoint> Building::areaForRadius(int radius) const
{
    return m_proto->areaForRadius(m_pos, m_orientation, radius, island());
}

bool Building::isWithinInfluenceRadius(Building *targetBuilding) const
{
    QSet<QPoint> influenceAreaSet = influenceArea().toSet();

    int countWithinArea = 0;
    int countTotal = 0;
    foreach(QPoint p, targetBuilding->coveredFields())
    {
        countTotal++;
        if(influenceAreaSet.contains(p))
            countWithinArea++;
    }

    return countWithinArea * 2 > countTotal;
}

QSet<Building *> Building::buildingsWithinInfluenceRadius() const
{
    QMap<Building*, int> containedFieldsPerBuilding;
    foreach(QPoint p, influenceArea())
        containedFieldsPerBuilding[island()->fieldAt(p).building]++;

    QSet<Building*> buildingsWithinInfluenceRadius;
    foreach(Building* building, containedFieldsPerBuilding.keys())
        if(containedFieldsPerBuilding[building] * 2 > building->coveredFieldsCount())
            buildingsWithinInfluenceRadius << building;

    return buildingsWithinInfluenceRadius;
}

QMap<int, int> Building::grantBuildingLicense() const
{
    return m_proto->grantLicenses;
}

int Building::takeGoods(GoodID good, int amount)
{
    Q_ASSERT(isTradeBuilding() || isProductionBuilding());

    if(isTradeBuilding())
    {
        // the goods might not be available anymore!
        int actualAmount = town()->currentStorageExcludingReservedOutgoing(good);
        bool success = town()->takeReservedOutgoing(good, actualAmount);
        Q_ASSERT(success);
        return actualAmount;
    }
    else
    {
        // the goods are guaranteed to be still available!
        Q_ASSERT(m_cs.production.outputFill >= amount);
        Q_ASSERT(m_cs.production.outputFillReservedOutgoing >= amount);
        m_cs.production.outputFill -= amount;
        m_cs.production.outputFillReservedOutgoing -= amount;
        return amount;
    }
}

QSet<Building *> Building::connectedBuildings() const
{
    QSet<Building*> buildings;
    foreach(int network, connectedRoadNetworks())
        buildings.unite(island()->buildingsInRoadNetwork(network));
    buildings.remove(const_cast<Building*>(this));
    return buildings;
}

Building::ConnectedBuildingsPaths Building::targetBuildings(Transportation *transportation) const
{
    ConnectedBuildingsPaths buildings;
    foreach(int network, connectedRoadNetworks())
    {
        QHash<uint, ConnectedBuildingsPaths> & reachableCacheNetwork = reachableCache[network];
        if(!reachableCacheNetwork.contains(transportation->prototypeID()))
        {
            // calculate reachable buildings using this type of transportation unit

            // buildings in this network we are interested in and are within the influence area
            ConnectedBuildingsPaths & reachableCacheTransportation = reachableCacheNetwork[transportation->prototypeID()];
            foreach(Building *building, island()->buildingsInRoadNetwork(network))
            {
                if(building != this && targetBuildingsFilter(building) && isWithinInfluenceRadius(building))
                {
                    // find source points belonging to this network
                    QList<QPoint> sources;
                    foreach(QPoint p, possibleStreetConnections())
                        if(island()->checkBounds(p) && island()->roadNetwork(p) == network)
                            sources << p;
                    // find target points belonging to this network
                    QList<QPoint> targets;
                    foreach(QPoint p, building->possibleStreetConnections())
                        if(island()->checkBounds(p) && island()->roadNetwork(p) == network)
                            targets << p;
                    // calculate the best route (do a search from each start point, TODO: improve this!)
                    Path bestPath;
                    bool foundARoute = false;
                    foreach(QPoint source, sources)
                    {
                        RouteCalculation routeCalculation(island(), transportation->routingCondition(),
                                               transportation->routingStrategy(),
                                               transportation->baseSpeed(), source, targets);
                        routeCalculation.run();
                        if(routeCalculation.routeFound())
                        {
                            Path newPath = routeCalculation.bestRoute();
                            if(!foundARoute || pathIsBetter(bestPath, newPath, transportation))
                                bestPath = newPath;
                            foundARoute = true;
                        }
                    }
                    // add the best path to the cache
                    if(foundARoute)
                        reachableCacheTransportation.insert(building, bestPath);
                }
            }
        }
        // Add to result, but check for duplicates, because some buildings can be connected using multiple different
        // road networks (take the shortest paths)
        foreachkv(Building *building, const Path & path, reachableCacheNetwork[transportation->prototypeID()])
        {
            if(buildings.contains(building)) {
                const Path & oldPath = buildings[building];
                if(pathIsBetter(oldPath, path, transportation))
                    buildings.insert(building, path);
            }
            else
                buildings.insert(building, path);
        }
    }
    return buildings;
}

bool Building::targetBuildingsFilter(Building *targetBuilding) const
{
    if(isTradeBuilding())
        return targetBuilding->isProductionBuilding();
    else if(isProductionBuilding())
        return m_proto->productionInputs.localGoods().contains(targetBuilding->m_proto->productionGood);
    else
        return false;
}

bool Building::pathIsBetter(const Path &oldPath, const Path &newPath, Transportation *transportation)
{
    return (transportation->routingStrategy() == ShortestPath)
            ? (newPath.time() < oldPath.time())
            : (newPath.length() < oldPath.length());
}

QSet<int> Building::connectedRoadNetworks() const
{
    QSet<int> roadNetworks;
    foreach(QPoint p, possibleStreetConnections())
        if(island()->checkBounds(p) && island()->roadNetwork(p) != -1)
            roadNetworks << island()->roadNetwork(p);
    return roadNetworks;
}

void Building::schedule()
{
    /*
    qDebug();
    qDebug("Schedule building \"%s\" %p:", qPrintable(m_proto->name), this);
    foreach(uint t, m_proto->transportationUnits.keys())
        qDebug(" - I have %d of those fuckin' %s!", t, qPrintable(TransportationLibrary::prototype(m_proto->transportationUnits[t])->propertyName));
    */

    switch(m_proto->buildingClass)
    {
    case BuildingPrototype::ClassProduction:
        // currently producing?
        if(m_cs.production.productionStarted != -1)
        {
            // production finished?
            if(game()->time() >= m_cs.production.productionStarted + m_proto->productionTime)
            {
                m_cs.production.outputFill++;
                m_cs.production.productionStarted = -1;
                emit goodProduced(m_proto->productionGood, 1);
            }
        }
        // currently not producing?
        if(m_cs.production.productionStarted == -1)
        {
            // can start production?
            bool canProduce = m_cs.production.outputFill < m_proto->productionCapacity;
            if(canProduce)
            {
                QMap<GoodID,int>::const_iterator it = m_proto->productionInputs.localGoods().constBegin();
                for(int i = 0; it != m_proto->productionInputs.localGoods().constEnd(); ++i, ++it)
                    canProduce &= m_cs.production.inputFills[i] >= it.value();
            }
            if(canProduce)
            {
                // start production
                for(int i = 0; i < m_proto->productionInputs.localGoods().count(); ++i)
                    m_cs.production.inputFills[i]--;
                m_cs.production.productionStarted = game()->time();
            }
        }
        // RESCHEDULE:
        // currently producing?
        if(m_cs.production.productionStarted != -1)
        {
            if(m_proto->productionInputs.localGoods().count() > 0)
                reschedule(20);
            else {
                Q_ASSERT(m_cs.production.productionStarted == game()->time());
                reschedule(m_proto->productionTime);
            }
        }
        else
        {
            reschedule(5);
        }
        break;

    case BuildingPrototype::ClassTrade:
        if(transportationUnits.length()) {
            collectGoods();
        }

    default:
        reschedule(10);
        break;
    }
}

void Building::deleteNow()
{
    delete this;
}

void Building::collectGoods()
{
    // get a free transportation unit and only continue if we have one
    Transportation *transportationUnit = 0;
    foreach(Transportation *t, transportationUnits)
        if(t->isIdle())
            transportationUnit = t;
    if(!transportationUnit)
        return;

    // trade buildings collect goods from production buildings
    if(isTradeBuilding())
    {
        // find building with the highest amount of available goods for collection which still fit into the town's storage
        ConnectedBuildingsPaths buildingsPaths = targetBuildings(transportationUnits.first());
        int highestCollectionAmount = 0;
        Building *buildingWithHighestCollectionAmount = 0;
        GoodID goodWithHighestCollectionAmount = 0;
        foreach(Building* building, buildingsPaths.keys())
        {
            Q_ASSERT(building->isProductionBuilding());
            GoodID good = building->m_proto->productionGood;
            int avail = building->m_cs.production.outputFill - building->m_cs.production.outputFillReservedOutgoing;
            int spaceLeft = town()->totalStorageCapacity() - town()->currentStorageIncludingReservedIncoming(good);
            int collectionAmount = qMin(avail, spaceLeft);

            if(collectionAmount > highestCollectionAmount)
            {
                highestCollectionAmount = collectionAmount;
                buildingWithHighestCollectionAmount = building;
                goodWithHighestCollectionAmount = good;
            }
        }

        if(buildingWithHighestCollectionAmount)
        {
            // collect the goods from the building providing the highest collection amount
            // (but no more than what fits in the transportation unit)
            LocalGoods collectGoods;
            collectGoods[goodWithHighestCollectionAmount] = qMin(highestCollectionAmount, transportationUnit->capacity());
            transportationUnit->startJob(buildingWithHighestCollectionAmount,
                                         buildingsPaths[buildingWithHighestCollectionAmount],
                                         LocalGoods(), collectGoods);
        }
    }
}


void Building::reserveIncoming(GoodID good, int amount)
{
    Q_ASSERT(isTradeBuilding());
    town()->reserveIncoming(good, amount);
}

void Building::addReservedIncoming(GoodID good, int amount)
{
    Q_ASSERT(isTradeBuilding());
    town()->addReservedIncoming(good, amount);
}

void Building::reserveOutgoing(GoodID good, int amount)
{
    Q_ASSERT(isTradeBuilding() || isProductionBuilding());

    if(isProductionBuilding())
    {
        Q_ASSERT(m_proto->productionGood == good);
        Q_ASSERT(m_cs.production.outputFill - m_cs.production.outputFillReservedOutgoing >= amount);
        m_cs.production.outputFillReservedOutgoing += amount;
    }
    else
    {
        town()->reserveOutgoing(good, amount);
    }
}
