#ifndef COMMANDINTERFACE_H
#define COMMANDINTERFACE_H

#include "global.h"
#include "namespace.h"
#include <QSharedPointer>

class QPoint;

OAE_BEGIN_NAMESPACE

class Command;
class Game;
class Town;
class Goods;
class LocalGoods;

class CommandInterface
{
public:
    CommandInterface(Game *game);

    void setGame(Game *game);

    QSharedPointer<Command> debug(Town *town);
    QSharedPointer<Command> addGoodsIsland(Town *town, LocalGoods goods);
    QSharedPointer<Command> createBuilding(Town *town, uint protoID, QPoint position, Orientation orientation);
    QSharedPointer<Command> createRoad(Town *town, uint protoID, QList<QPoint> fields);
    QSharedPointer<Command> deleteRoad(Town *town, QList<QPoint> fields);

private:
    Game *m_game;
};

OAE_END_NAMESPACE

#endif // COMMANDINTERFACE_H
