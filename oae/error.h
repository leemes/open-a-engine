#ifndef ERROR_H
#define ERROR_H

#include "global.h"
#include <QVariantList>

OAE_BEGIN_NAMESPACE

class Game;

class Error
{
public:
    enum Type {
        TooMuchPlayersInGame,
        RulesParseError
    };

    Game *game() const;
    Type type() const;
    QVariantList arguments() const;
    QVariant argumentAt(int i) const;

    static void report(Game *game, Type type, QVariantList args = QVariantList());
    static void report(Game *game, Type type, QVariant arg1);
    static void report(Game *game, Type type, QVariant arg1, QVariant arg2);
    static void report(Game *game, Type type, QVariant arg1, QVariant arg2, QVariant arg3);

private:
    Error(); // only report() can create an Error instance

    Game *m_game;
    Type m_type;
    QVariantList m_args;
};

OAE_END_NAMESPACE

#endif // ERROR_H
