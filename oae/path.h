#ifndef PATH_H
#define PATH_H

#include "global.h"
#include "namespace.h"
#include <QPoint>
#include <QList>
#include <QPair>

OAE_BEGIN_NAMESPACE

struct PathSegment
{
    QPoint from, to;
    qreal speed; //!< in fields per time tick
    qreal length() const; //!< in fields
    TimeF time() const; //!< in time ticks, but is of float type because of accuracy
    QPointF interpolate(qreal segmentProgress) const;
};

struct Path
{
    Path() : m_length(0.0), m_time(0.0) {}
    friend class RouteCalculation;

public:
    QList<PathSegment> segments; //!< If the path is virtually empty (start field == end field), there still exists a segment with those fields, zero length and zero time, to avoid a special case.
    QPointF interpolate(qreal progress) const;
    QPair<int,qreal> segmentForProgress(qreal progress) const; // tells the segment (.first) and subProgress within this segment (.second) for a given total path progress
    qreal length() const;
    TimeF time() const;
    QPoint from() const;
    QPoint to() const;

private:
    void precompute();
    QList<qreal> segmentsProgress; // cache: telling at which progress which segment starts
    qreal m_length;
    TimeF m_time;
};

OAE_END_NAMESPACE

#endif // PATH_H
