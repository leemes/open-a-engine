#ifndef GAME_H
#define GAME_H

#include "global.h"
#include <QObject>
#include <QSize>
#include "namespace.h"

OAE_BEGIN_NAMESPACE

class Player;
class World;
class OpenAEngine;
class Error;
class Manager;
class Scheduler;

class Game : public QObject
{
    Q_OBJECT

public:
    explicit Game(OpenAEngine *engine, QSize size);

public slots:
    OpenAEngine *engine() const;
    Manager *manager() const;
    Scheduler *scheduler() const;
    World *world() const;
    QList<Player*> players() const;
    Player *thisPlayer() const;
    PlayerID thisPlayerId() const;

    Time time() const;
    TimeF timeF() const;

    Player *newPlayer();

    // initial game settings
    void setInitialCoins(int coins);

    void start();

private:
    static const uint maxPlayers = 8;

    OpenAEngine *m_engine;
    Manager *m_manager;
    World *m_world;
    Player *m_players[8];

    uint m_numPlayers;

    // initial game settings
    int m_initialCoins;
};

OAE_END_NAMESPACE

#endif // GAME_H
