#ifndef LIMITS_H
#define LIMITS_H

#include "global.h"
#include <qglobal.h>


#define OAE_MAX_BUILDING_LICENSES   24

#define OAE_BITS_TERRAIN_TYPES      16
#define OAE_BITS_ROAD_TYPES         16
#define OAE_BITS_PLAYER_IDS         8


OAE_BEGIN_NAMESPACE

class Limits
{
    Limits() {}

public:
    static const int maxPlayers = 8;
    static const int maxBuildingProductionInputs = 8;
    static const int maxStorageCapacity = 999;
};

OAE_END_NAMESPACE

#endif // LIMITS_H
