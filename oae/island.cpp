#include "island.h"
#include "world.h"
#include "town.h"
#include "game.h"
#include "player.h"
#include "building.h"
#include "prototypes/buildingprototype.h"
#include "internal/datastructures/sparsebitarray2d.h"
#include "internal/hashfunctions.h"
#include "internal/routing/routecalculation.h"
#include <QDebug>

static const QPoint directionVectors[] = { QPoint(1, 0), QPoint(-1, 0), QPoint(0, 1), QPoint(0, -1) };


Island::FieldData::FieldData() :
    terrain(0),
    roadType(0),
    roadNetworkID(-1),
    building(0),
    owner(0),
    buildingLicenses(0)
{
}



Island::RoadNetworkData::RoadNetworkData() :
    successorID(-1)
{
}

Island::RoadNetworkData::RoadNetworkData(QPoint representativeField) :
    representativeField(representativeField),
    successorID(-1)
{
}



Island::Island(World *world, QRect boundaries) :
    QObject(world),
    m_boundaries(boundaries),
    m_w(boundaries.width()),
    m_h(boundaries.height()),
    m_s(m_w * m_h),
    m_fields(new FieldData[m_s]),
    m_nextRoadNetworkID(0)
{
}

Island::~Island()
{
    foreach(Town *town, m_towns)
        delete town;

    delete[] m_fields;
}

World *Island::world() const
{
    return qobject_cast<World*>(parent());
}

Game *Island::game() const
{
    return world()->game();
}

QList<Town *> Island::towns() const
{
    return m_towns;
}

Town *Island::townOfThisPlayer() const
{
    foreach(Town *town, m_towns)
        if(town->player() == game()->thisPlayer())
            return town;
    return 0;
}

QRect Island::boundaries() const
{
    return m_boundaries;
}

Island::BuildabilityStatus Island::checkBuildability(PlayerID player, BuildingPrototype *proto, QPoint pos, Orientation orientation) const
{
    // Check if fully on island
    foreach(QPoint p, proto->coveredFields(pos, orientation))
        if(!checkBounds(p))
            return NotBuildableNotOnIsland;

    // Check for correct terrain
    // TODO

    // Check for collisions
    foreach(QPoint p, proto->coveredFields(pos, orientation))
        if(fieldAt(p).hasBuilding() || fieldAt(p).hasRoad())
            return NotBuildableCollision;

    // Check for building licenses
    if(proto->requireLicenseOverlap)
    {
        bool foundOverlap = false;

        // the radius in which an overlap has to occur is the maximum radius of the granted licenses
        QList<int> radii = proto->grantLicenses.values();
        int radius = *std::max_element(radii.begin(), radii.end());

        // an overlap has to occur with any required license
        foreach(QPoint p, proto->areaForRadius(pos, orientation, radius, this)) {
            if(fieldAt(p).hasOneOfLicenses(proto->requireLicenses)) {
                foundOverlap = true;
                break;
            }
        }

        if(!foundOverlap)
            return NotBuildableLicense;
    }
    else
    {
        int numFieldsWithLicense = 0;
        foreach(QPoint p, proto->coveredFields(pos, orientation))
            if(fieldAt(p).hasOneOfLicenses(proto->requireLicenses))
                numFieldsWithLicense++;

        // more than the half of the covered fields have to have the required license(s)
        if(numFieldsWithLicense * 2 <= proto->coveredFieldsCount())
            return NotBuildableLicense;
    }

    return Buildable;
}

Island::BuildabilityStatus Island::checkBuildability(PlayerID player, RoadPrototype *proto, QPoint pos) const
{
    // Check for correct terrain
    // TODO

    // Check for collisions
    if(fieldAt(pos).hasBuilding())
        return NotBuildableCollision;

    // Check for building licenses
    if(fieldAt(pos).owner != player ||
            !fieldAt(pos).hasOneOfLicenses(proto->requireLicenses))
        return NotBuildableLicense;

    return Buildable;
}

QList<QPoint> Island::shortestBuildableRoadPath(PlayerID player, RoadPrototype *proto, QPoint source, QPoint target) const
{
    QList<QPoint> points;
    // source and target fields have to be buildable
    if(checkBuildability(player, proto, source) == Buildable && checkBuildability(player, proto, target) == Buildable)
    {
        // calculate the shortest path
        RouteCalculation rouceCalc(this, getRoutingConditionFunctionForName("road buildability"),
                                   FastestPath, 1.0, source, QList<QPoint>() << target);
        rouceCalc.setRoutingConditionInternalData(reinterpret_cast<void*>(proto));
        rouceCalc.run();

        if(rouceCalc.routeFound())
        {
            Path path = rouceCalc.bestRoute();
            points << source;
            if(path.length() != 0)
                foreach(PathSegment segment, path.segments)
                    points << segment.to;
        }
    }
    return points;
}

uint Island::road(QPoint pos) const
{
    Q_ASSERT(checkBounds(pos));
    return fieldAt(pos).roadType;
}

void Island::setRoad(QPoint pos, uint road)
{
    Q_ASSERT(checkBounds(pos));

    uint oldRoad = fieldAt(pos).roadType;
    if(oldRoad == road)
        return;

    field(pos).roadType = road;

    // What happened?
    bool roadCreated = (road != 0 && oldRoad == 0);
    bool roadChanged = (road != 0 && oldRoad != 0);
    bool roadDeleted = (road == 0 && oldRoad != 0);


    if(roadCreated)
    {
        // the set of adjacent road networks (they are already final IDs)
        QSet<int> adjacentNetworks;
        for(int i = 0; i < 4; ++i)
            if(checkBounds(pos + directionVectors[i]) && field(pos + directionVectors[i]).hasRoad())
                adjacentNetworks << roadNetwork(pos + directionVectors[i]);

        // road placed in an empty neighborhood, but maybe there are some buildings
        if(adjacentNetworks.count() == 0)
        {
            // assign and place new networkID on this field
            int newNetwork = makeNetwork(pos);

            // we immediately know which buildings belong to the new network (look around this field)
            QSet<Building*> newBuildings;
            for(int i = 0; i < 4; ++i)
                if(checkBounds(pos + directionVectors[i]) && field(pos + directionVectors[i]).hasBuilding())
                    newBuildings << field(pos + directionVectors[i]).building;
            m_roadNetworks[newNetwork].buildings = newBuildings;
        }

        // a network has been extended, maybe including a new building
        else if(adjacentNetworks.count() == 1)
        {
            int oldNetwork = *adjacentNetworks.constBegin();

            // check if there is a building around this field
            QSet<Building*> newBuildings;
            for(int i = 0; i < 4; ++i)
                if(checkBounds(pos + directionVectors[i]) && field(pos + directionVectors[i]).hasBuilding())
                    newBuildings << field(pos + directionVectors[i]).building;

            // optimize for trivial extensions without new buildings in the network
            // TODO: This can't be done if we want to ensure that the shortest paths are final for each network ID!
            if(newBuildings.isEmpty() || m_roadNetworks[oldNetwork].buildings.contains(newBuildings))
            {
                // only place the networkID on this field
                field(pos).roadNetworkID = oldNetwork;
            }
            else
            {
                // assign and place new networkID on this field
                int newNetwork = makeNetwork(pos);

                // we immediately know which buildings belong to the new network (the old ones + look around this field)
                newBuildings.unite(m_roadNetworks[oldNetwork].buildings);
                m_roadNetworks[newNetwork].buildings = newBuildings;

                // set the successor of the old network to the new network
                m_roadNetworks[oldNetwork].setSuccessor(newNetwork);
            }
        }

        // two or more networks have been joined together
        else // if(adjacentNetworks.count() >= 2)
        {
            // assign and place new networkID on this field
            int newNetwork = makeNetwork(pos);

            // we immediately know which buildings belong to the new network (the union + look around this field)
            QSet<Building*> newBuildings;
            for(int i = 0; i < 4; ++i)
                if(checkBounds(pos + directionVectors[i]) && field(pos + directionVectors[i]).hasBuilding())
                    newBuildings << field(pos + directionVectors[i]).building;
            foreach(int oldNetwork, adjacentNetworks)
                newBuildings.unite(m_roadNetworks[oldNetwork].buildings);
             m_roadNetworks[newNetwork].buildings = newBuildings;

            // set the successors of the old networks to the new network
            foreach(int oldNetwork, adjacentNetworks)
                m_roadNetworks[oldNetwork].setSuccessor(newNetwork);
        }
    }

    if(roadChanged)
    {
        // as the shortest paths within the road network may have changed, we need to update the road network ID
        // and simply copy the set of buildings to the new network

        int oldNetwork = finalNetwork(field(pos).roadNetworkID);

        // assign and place new networkID on this field
        int newNetwork = makeNetwork(pos);

        // we immediately know which buildings belong to the new network (the old ones)
        m_roadNetworks[newNetwork].buildings = m_roadNetworks[oldNetwork].buildings;

        // set the successors of the old network to the new network
        m_roadNetworks[oldNetwork].setSuccessor(newNetwork);
    }

    if(roadDeleted)
    {
        int oldNetwork = finalNetwork(field(pos).roadNetworkID);
        field(pos).roadNetworkID = -1;

        // calculate the set of adjacent road fields in which the network continues
        QSet<QPoint> adjacentRoadFields;
        for(int i = 0; i < 4; ++i)
            if(checkBounds(pos + directionVectors[i]) && field(pos + directionVectors[i]).hasRoad())
                adjacentRoadFields << pos + directionVectors[i];

        // the field of road was in an empty neighborhood
        if(adjacentRoadFields.count() == 0)
        {
            // do nothing
        }

        // a network has been shrinked, maybe excluding a building which was connected only to this particular field
        else if(adjacentRoadFields.count() == 1)
        {
            QPoint adjacentRoadField = *adjacentRoadFields.constBegin();

            // calculate the set of adjacent buildings of this particular field (including those which are also adjacent
            // to remaining fields of the road network) - while saving the field in which the road network continues
            QSet<Building*> disconnectedBuildings;
            for(int i = 0; i < 4; ++i)
            {
                if(checkBounds(pos + directionVectors[i]))
                {
                    if(field(pos + directionVectors[i]).hasBuilding())
                        disconnectedBuildings << field(pos + directionVectors[i]).building;
                }
            }

            // remove the buildings from this set which are also adjacent to remaining fields of the road network
            for(int i = 0; i < 4; ++i)
                if(checkBounds(adjacentRoadField + directionVectors[i]) && field(adjacentRoadField + directionVectors[i]).hasBuilding())
                    disconnectedBuildings.remove(field(adjacentRoadField + directionVectors[i]).building);

            // if the remaining set of buildings is empty this means that we didn't change anything - otherwise we need
            // to introduce a new networkID from which we remove those buildings
            if(disconnectedBuildings.isEmpty())
            {
                // do nothing
            }
            else
            {
                // assign and place new networkID on the adjacent field
                int newNetwork = makeNetwork(adjacentRoadField);

                // we immediately know which buildings belong to the new network (the old ones - the disconnected ones)
                m_roadNetworks[newNetwork].buildings = m_roadNetworks[oldNetwork].buildings - disconnectedBuildings;

                // this counts for the whole old network, so just set the successor
                m_roadNetworks[oldNetwork].setSuccessor(newNetwork);
            }
        }

        // two or more networks have been split
        else // if(adjacentRoadFields.count() >= 2)
        {
            // reconstruct the remaining network in every adjacent direction
            foreach(QPoint adjacentRoadField, adjacentRoadFields)
            {
                // a field might already have been updated by another field (when the network contained a loop)
                if(finalNetwork(field(adjacentRoadField).roadNetworkID) == oldNetwork)
                {
                    // assign and place new networkID on the adjacent field
                    int newNetwork = makeNetwork(adjacentRoadField);

                    // travel along the whole connected roads to update the networkID, since this cannot be done by
                    // simply assigning a successor
                    traverseAndSetNewRoadNetwork(newNetwork);
                }
            }
        }
    }

    Q_ASSERT(field(pos).hasRoad() == (road != 0));

    emit reportRoadChanged(this, pos, road);
}

void Island::registerNewTown(Town *town)
{
    m_towns << town;
    connect(town, SIGNAL(reportNewBuilding(Building*)),
            this, SIGNAL(reportNewBuilding(Building*)));
    connect(town, SIGNAL(reportNewBuilding(Building*)),
            this, SLOT(onNewBuilding(Building*)));
}

void Island::registerDeleteTown(Town *town)
{
    m_towns.removeOne(town);
}

void Island::dumpRoadNetworks() const
{
    for(int i = 0; i < m_roadNetworks.count(); ++i)
    {
        qDebug("Road network %d: position: %d|%d", i,
               m_roadNetworks[i].representativeField.x(),
               m_roadNetworks[i].representativeField.y());

        if(m_roadNetworks[i].hasSuccessor())
            qDebug("  succ: %d", m_roadNetworks[i].successorID);
        else
            qDebug("  no succ");

        if(m_roadNetworks[i].buildings.count())
            foreach(Building *building, m_roadNetworks[i].buildings)
                qDebug() << " " << building;
        else
            qDebug("  no building");
    }
    qDebug();
}

int Island::roadNetwork(QPoint pos)
{
    int network = field(pos).roadNetworkID;
    if(network != -1 && m_roadNetworks[network].hasSuccessor())
        network = field(pos).roadNetworkID = finalNetwork(network);
    return network;
}

QSet<Building*> Island::buildingsInRoadNetwork(int network)
{
    network = finalNetwork(network);
    return m_roadNetworks[network].buildings;
}

bool Island::roadNetworkStillExists(int network)
{
    return !m_roadNetworks[network].hasSuccessor();
}

int Island::finalNetwork(int networkID) const
{
    while(m_roadNetworks[networkID].hasSuccessor())
        networkID = m_roadNetworks[networkID].successorID;
    return networkID;
}

int Island::makeNetwork(QPoint pos)
{
    m_roadNetworks.append(RoadNetworkData(pos));
    return field(pos).roadNetworkID = m_nextRoadNetworkID++;
}

void Island::traverseAndSetNewRoadNetwork(int networkID)
{
    QPoint pos = m_roadNetworks[networkID].representativeField;
    QSet<Building*> buildings;

    // set the networkID on this field
    field(pos).roadNetworkID = networkID;

    // start looking for buildings at this field (in all 4 directions)
    for(int i = 0; i < 4; ++i)
        if(checkBounds(pos + directionVectors[i]) && field(pos + directionVectors[i]).hasBuilding())
            buildings << field(pos + directionVectors[i]).building;

    // use depth first search to follow the whole network
    SparseBitArray2D visited(boundaries().size());
    visited.setBitAt(pos);
    for(int i = 0; i < 4; ++i)
        if(checkBounds(pos + directionVectors[i]) && field(pos + directionVectors[i]).hasRoad())
            traverseAndSetNewRoadNetworkDFS(networkID, pos, pos + directionVectors[i], visited, buildings);

    // save the results
    m_roadNetworks[networkID].buildings = buildings;
}

void Island::traverseAndSetNewRoadNetworkDFS(int networkID, QPoint comingFrom, QPoint current,
                                             SparseBitArray2D &visited, QSet<Building*> &buildings)
{
    // there are three possible neighbors
    QPoint offset = current - comingFrom;
    Q_ASSERT(offset.manhattanLength() == 1);
    QPoint offsetLeft(offset.y(), -offset.x()); // rotated 90 degrees to the left
    QPoint next[3] = { current + offset, current + offsetLeft, current - offsetLeft };

    // set the networkID on this field
    field(current).roadNetworkID = networkID;

    // process myself by looking for buildings in the three directions
    for(int i = 0; i < 3; ++i)
        if(checkBounds(next[i]) && field(next[i]).hasBuilding())
            buildings << field(next[i]).building;

    // mark myself as visited
    visited.setBitAt(current);

    // traverse neighbours using depth first search
    for(int i = 0; i < 3; ++i)
        if(checkBounds(next[i]) && field(next[i]).hasRoad() && !visited.bitAt(next[i]))
            traverseAndSetNewRoadNetworkDFS(networkID, current, next[i], visited, buildings);
}

void Island::onNewBuilding(Building *building)
{
    // place the building pointer on all covered fields
    foreach(QPoint p, building->coveredFields())
        field(p).building = building;

    // tell the now connected road networks about the new building
    QSet<int> connectedRoadNetworks;
    foreach(QPoint p, building->possibleStreetConnections())
        if(checkBounds(p) && field(p).hasRoad())
            connectedRoadNetworks << finalNetwork(field(p).roadNetworkID);
    foreach(int oldNetwork, connectedRoadNetworks)
    {
        // introduce a new network at the old representative field
        int newNetwork = makeNetwork(m_roadNetworks[oldNetwork].representativeField);

        // we immediately know which buildings belong to the new network (the old ones + the new one)
        QSet<Building*> newBuildings = m_roadNetworks[oldNetwork].buildings;
        newBuildings << building;
        m_roadNetworks[newNetwork].buildings = newBuildings;

        // set the successor of the old network to the new network
        m_roadNetworks[oldNetwork].setSuccessor(newNetwork);
    }

    // grant building licenses
    foreachkv(int license, int radius, building->grantBuildingLicense())
    {
        QList<QPoint> area = building->areaForRadius(radius);
        foreach(QPoint p, area)
        {
            if(checkBounds(p)) {
                FieldData &f = field(p);
                f.owner = building->player()->id();
                f.grantLicenses(license);
            }
        }
    }
}

void Island::onDeleteBuilding(Building *building)
{
    // TODO
}


