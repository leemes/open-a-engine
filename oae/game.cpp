#include "game.h"
#include "world.h"
#include "player.h"
#include "error.h"
#include "openaengine.h"
#include "namespace.h"
#include "manager.h"


Game::Game(OpenAEngine *engine, QSize size) :
    QObject(engine),
    m_engine(engine),
    m_manager(new Manager(this)),
    m_world(new World(this, size)),
    m_numPlayers(0)
{
    for(uint i = 0; i < maxPlayers; ++i)
        m_players[i] = 0;
}

OpenAEngine *Game::engine() const
{
    return m_engine;
}

Manager *Game::manager() const
{
    return m_manager;
}

Scheduler *Game::scheduler() const
{
    return m_manager->scheduler();
}

World *Game::world() const
{
    return m_world;
}

QList<Player *> Game::players() const
{
    QList<Player*> players;
    for(uint i = 0; i < m_numPlayers; ++i)
        players << m_players[i];
    return players;
}

Player *Game::thisPlayer() const
{
    return m_players[0];
}

PlayerID Game::thisPlayerId() const
{
    return thisPlayer()->id();
}

Time Game::time() const
{
    return m_manager->time();
}

TimeF Game::timeF() const
{
    return m_manager->timeF();
}

Player *Game::newPlayer()
{
    if(m_numPlayers == maxPlayers)
        Error::report(this, Error::TooMuchPlayersInGame);

    Player *player = new Player(this, m_numPlayers);
    m_players[m_numPlayers++] = player;

    return player;
}

void Game::setInitialCoins(int coins)
{
    m_initialCoins = coins;
}

void Game::start()
{
    // initial settings
    foreach(Player *p, players())
        p->addCoins(m_initialCoins);

    // start the game
    m_manager->start();
}
