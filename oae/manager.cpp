#include "manager.h"
#include "game.h"
#include "command.h"
#include "internal/scheduler.h"
#include <QTimer>


Manager::Manager(Game *game) :
    QObject(game),
    m_scheduler(new Scheduler(this)),
    m_timer(new QTimer(this)),
    m_time(0),
    m_delay(initialDelay),
    m_speed(1.0)
{
    setDefaultSpeed();
    connect(m_timer, SIGNAL(timeout()), SLOT(iterate()));
}

Game *Manager::game() const
{
    return qobject_cast<Game*>(parent());
}

Scheduler *Manager::scheduler() const
{
    return m_scheduler;
}

Time Manager::time() const
{
    return m_time;
}

TimeF Manager::timeF() const
{
    qreal subTick = (qreal)m_elapsedAfterLastTick.elapsed() / m_timer->interval();
    subTick = qMin(1.0, subTick);
    return m_time + subTick;
}

void Manager::enqueueCommand(QSharedPointer<Command> cmd)
{
    int time = m_time + m_delay;
    cmd->m_time = time;
    while(m_queue.count() <= m_delay - 1)
        m_queue.enqueue(QList<QSharedPointer<Command> >());
    m_queue[m_delay - 1].append(cmd);
}

void Manager::iterate()
{
    m_time++;
    //if(m_time % 10 == 0)
    //    qDebug("game time = %d", m_time);

    m_elapsedAfterLastTick.restart();

    // fetch the commands at the head of the queue (for the now current time slot)
    QList<QSharedPointer<Command> > commands;
    if(!m_queue.isEmpty())
        commands = m_queue.dequeue();

    // execute these commands
    foreach(QSharedPointer<Command> cmd, commands)
        cmd->execute();

    // update the model's state
    m_scheduler->iterate();
}

void Manager::start()
{
    m_timer->start();
}

void Manager::pause()
{
    m_timer->stop();
}

bool Manager::running() const
{
    return m_timer->isActive();
}

void Manager::setDefaultSpeed()
{
    setSpeed(1.0);
}

void Manager::setSpeed(float speed)
{
    m_speed = speed;
    qDebug("set speed to %f", m_speed);
    m_timer->setInterval(msPerTimeUnit / speed);
}

float Manager::speed() const
{
    return m_speed;
}
