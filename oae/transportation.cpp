#include "transportation.h"
#include "prototypes/transportationprototype.h"
#include "building.h"
#include <cmath>


Transportation::Transportation(Building *building, TransportationPrototype *prototype) :
    QObject(building),
    Scheduled(building->game(), 0),
    m_proto(prototype),
    m_client(0),
    m_state(StateIdle),
    m_location(LocationHome),
    m_lastStateChange(0)
{
}

uint Transportation::prototypeID() const
{
    return m_proto->id;
}

RoutingConditionFunc Transportation::routingCondition() const
{
    return m_proto->routingCondition;
}

RoutingStrategy Transportation::routingStrategy() const
{
    return m_proto->routingStrategy;
}

qreal Transportation::baseSpeed() const
{
    return m_proto->baseSpeed;
}

int Transportation::capacity() const
{
    return m_proto->capacity;
}

Building *Transportation::home() const
{
    return qobject_cast<Building*>(parent());
}

Building *Transportation::client() const
{
    return m_client;
}

Transportation::Location Transportation::location() const
{
    return m_location;
}

Transportation::State Transportation::state() const
{
    return m_state;
}

Time Transportation::lastStateChange() const
{
    return m_lastStateChange;
}

bool Transportation::isIdle() const
{
    Q_ASSERT((m_location == LocationHome) == (m_state == StateIdle));
    return m_state == StateIdle;
}

bool Transportation::isBusy() const
{
    return !isIdle();
}

Building *Transportation::sourceBuilding() const
{
    if(m_location == LocationHome)
        return 0;
    else if(m_location == LocationHomeToClient)
        return home();
    else
        return client();
}

Building *Transportation::targetBuilding() const
{
    if(m_location == LocationHome)
        return 0;
    else if(m_location == LocationHomeToClient)
        return client();
    else
        return home();
}

QPoint Transportation::sourceField() const
{
    if(m_location == LocationHome)
        return QPoint(0, 0);
    else if(m_location == LocationHomeToClient)
        return m_path.from();
    else
        return m_path.to();
}

QPoint Transportation::targetField() const
{
    if(m_location == LocationHome)
        return QPoint(0, 0);
    else if(m_location == LocationHomeToClient)
        return m_path.to();
    else
        return m_path.from();
}

const Path &Transportation::path() const
{
    Q_ASSERT(isBusy());
    return m_path;
}

qreal Transportation::progress() const
{
    switch(m_state) {
    case StateIdle:
    case StateStart:
        return 0.0;
    case StateStop:
        return 1.0;
    default:
        // we need to limit this value to 1.0 since the path time was rounded up when used as the reschedule timeout
        return qMin(1.0, qreal(gameTimeF() - m_lastStateChange) / m_path.time());
    }
}

LocalGoods Transportation::goods() const
{
    return m_goods;
}

void Transportation::startJob(Building *client, Path path, LocalGoods deliver, LocalGoods collect)
{
    Q_ASSERT(isIdle());

    m_client = client;
    m_path = path;
    m_collect = collect; // save the goods to be collected for later use

    m_location = LocationHomeToClient;
    m_state = StateStart;
    actionStartDelivery(deliver);
    reschedule(m_proto->delays.startHome);
}

void Transportation::schedule()
{
    Q_ASSERT(isBusy());

    if(m_location == LocationHomeToClient) {
        if(m_state == StateStart) {
            m_state = StateRun;
            m_lastStateChange = gameTime();
            reschedule(ceil(m_path.time()));
        } else if(m_state == StateRun) {
            m_state = StateStop;
            m_lastStateChange = gameTime();
            reschedule(m_proto->delays.stopClient);
        } else if(m_state == StateStop) {
            m_location = LocationClientToHome;
            m_state = StateStart;
            m_lastStateChange = gameTime();
            actionStopDelivery();
            actionStartCollection();
            reschedule(m_proto->delays.startClient);
        } else
            qFatal("State machine error");
    } else if(m_location == LocationClientToHome) {
        if(m_state == StateStart) {
            m_state = StateRun;
            m_lastStateChange = gameTime();
            reschedule(ceil(m_path.time()));
        } else if(m_state == StateRun) {
            m_state = StateStop;
            m_lastStateChange = gameTime();
            reschedule(m_proto->delays.stopHome);
        } else if(m_state == StateStop) {
            m_location = LocationHome;
            m_state = StateIdle;
            m_lastStateChange = gameTime();
            actionStopCollection();
            actionFinished();
            Q_ASSERT(isIdle());
        } else
            qFatal("State machine error");
    } else
        qFatal("State machine error");
}

void Transportation::deleteNow()
{
    delete this;
}

void Transportation::actionStartDelivery(const LocalGoods & goods)
{
    //qDebug("actionStartDelivery at time = %d", gameTime());
    foreachkv(GoodID good, int amount, goods)
    {
        int actualAmount = home()->takeGoods(good, amount);
        client()->reserveIncoming(good, actualAmount);
        m_goods[good] = actualAmount; // we need to remember the actual amount
        //qDebug("take good to deliver: %d, amount: %d (requested: %d)", good, actualAmount, amount);
    }
    // For the collection, reserve both the incoming in the home and the outgoing in the client building.
    foreachkv(GoodID good, int amount, m_collect)
    {
        home()->reserveIncoming(good, amount);
        client()->reserveOutgoing(good, amount);
    }
}

void Transportation::actionStopDelivery()
{
    //qDebug("actionStopDelivery at time = %d", gameTime());
    // In the current implementations, goods which don't fit in the client building get lost.
    foreachkv(GoodID good, int amount, m_goods)
        client()->addReservedIncoming(good, amount);
    m_goods.clear();
}

void Transportation::actionStartCollection()
{
    //qDebug("actionStartCollection at time = %d", gameTime());
    Q_ASSERT(m_goods.isEmpty());
    foreachkv(GoodID good, int amount, m_collect)
    {
        int actualAmount = client()->takeGoods(good, amount);
        // In the current implementation, the goods to be collected are reserved when starting the delivery. The current
        // implementation further guarantees that this reservation is hold until now.
        Q_ASSERT(actualAmount == amount);
        //qDebug("collect good: %d, amount: %d", good, actualAmount);
    }
    m_goods = m_collect;
    m_collect.clear(); // we do not need this information anymore
}

void Transportation::actionStopCollection()
{
    //qDebug("actionStopCollection at time = %d", gameTime());
    // In the current implementations, goods which don't fit in the home building get lost.
    foreachkv(GoodID good, int amount, m_goods)
        home()->addReservedIncoming(good, amount);
    m_goods.clear();
}

void Transportation::actionFinished()
{
    m_path = Path();
    m_client = 0;
}
