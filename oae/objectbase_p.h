#ifndef OBJECTBASE_P_H
#define OBJECTBASE_P_H

#include <QtGlobal>

class ObjectBase;
class ObjectBasePrivate
{
    Q_DECLARE_PUBLIC(ObjectBase)

protected:
    ObjectBase * const q_ptr;
    ObjectBasePrivate(ObjectBase *q);
};

#endif // OBJECTBASE_P_H
