#include "objectbase.h"
#include "objectbase_p.h"


ObjectBase::ObjectBase(QObject *parent) :
    QObject(parent),
    d_ptr(new ObjectBasePrivate(this))
{
}

ObjectBase::~ObjectBase()
{
}


ObjectBasePrivate::ObjectBasePrivate(ObjectBase *q) :
    q_ptr(q)
{
}
