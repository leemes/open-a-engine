#include "path.h"
#include "internal/euclideanlength.h"


qreal PathSegment::length() const
{
    return euclideanLength(to - from);
}

qreal PathSegment::time() const
{
    return length() / speed;
}

QPointF PathSegment::interpolate(qreal segmentProgress) const
{
    return QPointF(from) * (1.0 - segmentProgress) + QPointF(to) * segmentProgress;
}

QPointF Path::interpolate(qreal progress) const
{
    if(progress <= 0.0)
        return segments.first().from;
    else if(progress >= 1.0)
        return segments.last().to;
    else {
        int segmentIndex = 0;
        while(segmentIndex < segments.count() - 1 && segmentsProgress[segmentIndex + 1] < progress)
            segmentIndex++;
        qreal subProgress = progress - segmentsProgress[segmentIndex];
        if(segmentIndex == segments.count() - 1)
            subProgress /= 1.0 - segmentsProgress[segmentIndex];
        else
            subProgress /= segmentsProgress[segmentIndex + 1] - segmentsProgress[segmentIndex];
        return segments[segmentIndex].interpolate(subProgress);
    }
}

QPair<int, qreal> Path::segmentForProgress(qreal progress) const
{
    if(progress <= 0.0)
        return qMakePair(0, 0.0);
    else if(progress >= 1.0)
        return qMakePair(segments.count() - 1, 1.0);
    else {
        int segmentIndex = 0;
        while(segmentIndex < segments.count() - 1 && segmentsProgress[segmentIndex + 1] < progress)
            segmentIndex++;
        qreal subProgress = progress - segmentsProgress[segmentIndex];
        if(segmentIndex == segments.count() - 1)
            subProgress /= 1.0 - segmentsProgress[segmentIndex];
        else
            subProgress /= segmentsProgress[segmentIndex + 1] - segmentsProgress[segmentIndex];
        return qMakePair(segmentIndex, subProgress);
    }
}

qreal Path::length() const
{
    return m_length;
}

TimeF Path::time() const
{
    return m_time;
}

QPoint Path::from() const
{
    return segments.first().from;
}

QPoint Path::to() const
{
    return segments.last().to;
}

void Path::precompute()
{
    m_time = m_length = 0.0;
    foreach(PathSegment seg, segments) {
        m_time += seg.time();
        m_length += seg.length();
    }
    qreal currentTime = 0.0;
    foreach(PathSegment seg, segments) {
        segmentsProgress << (currentTime / m_time);
        currentTime += seg.time();
    }
    Q_ASSERT(currentTime == m_time);
}


