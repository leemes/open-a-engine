#ifndef BUILDING_H
#define BUILDING_H

#include "global.h"
#include <QObject>
#include <QPoint>
#include <QHash>
#include "namespace.h"
#include "limits.h"
#include "path.h"
#include "internal/scheduled.h"

class QRect;
class QIcon;
class QImage;

OAE_BEGIN_NAMESPACE

class Island;
class Town;
class Player;
class Game;
class BuildingPrototype;
class Transportation;

class Building : public QObject, public Scheduled
{
    Q_OBJECT
    friend class Command_Debug;

public:
    explicit Building(Island *island, Town *town, BuildingPrototype *prototype, QPoint position, Orientation orientation);

    Island *island() const;
    Town *town() const;
    Player *player() const;
    Game *game() const;

    QString typeName() const;

    // geometry within island (except globalBoundaries())
    QPoint position() const;
    Orientation orientation() const;
    QRect boundaries() const;
    QRect globalBoundaries() const;
    QList<QPoint> coveredFields() const;
    int coveredFieldsCount() const; // inexpensive shorthand for coveredFields().count()
    QList<QPoint> possibleStreetConnections() const;

    // building class
    bool isTradeBuilding() const;
    bool isProductionBuilding() const;

    // properties from prototype
    QImage icon() const;
    int storageCapacity() const;
    float influenceRadius() const;
    QList<QPoint> influenceArea() const;
    QList<QPoint> areaForRadius(int radius) const;
    bool isWithinInfluenceRadius(Building *targetBuilding) const;
    QSet<Building*> buildingsWithinInfluenceRadius() const; // expensive operation!
    QMap<int, int> grantBuildingLicense() const; // key: license, value: radius

    // interaction
    int takeGoods(GoodID good, int amount);
    void addGoods(GoodID good, int amount);
    void reserveIncoming(GoodID good, int amount);
    void addReservedIncoming(GoodID good, int amount);
    void reserveOutgoing(GoodID good, int amount);

signals:
    // events:
    void goodProduced(GoodID good, int amount);

private:
    void schedule();
    void deleteNow();

    // Periodic tasks
    void collectGoods(); // trade buildings and production buildings with inputs

    typedef QHash<Building*,Path> ConnectedBuildingsPaths;

    // ...
    QSet<int> connectedRoadNetworks() const;
    QSet<Building*> connectedBuildings() const;
    ConnectedBuildingsPaths targetBuildings(Transportation *transportation) const;
    bool targetBuildingsFilter(Building* targetBuilding) const; // used as a filter function
    static bool pathIsBetter(const Path & oldPath, const Path & newPath, Transportation *transportation);

private:
    Town *m_town;
    BuildingPrototype *m_proto;
    QPoint m_pos;
    Orientation m_orientation;

public: // TODO
    QList<Transportation*> transportationUnits;
    mutable QHash<int /* roadNetwork */, QHash<uint /* transportationProtoID */, ConnectedBuildingsPaths> > reachableCache;

    union ClassSpecific {
        struct ClassSpecificProduction {
            char inputFills[Limits::maxBuildingProductionInputs];
            Time productionStarted;
            char outputFill;
            char outputFillReservedOutgoing;
        } production;
    } m_cs;
};

OAE_END_NAMESPACE

#endif // BUILDING_H
