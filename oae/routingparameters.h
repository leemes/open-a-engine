#ifndef ROUTINGPARAMETERS_H
#define ROUTINGPARAMETERS_H

#include "global.h"
#include "island.h"
#include "libraries/roadlibrary.h"
#include "prototypes/roadprototype.h"

OAE_BEGIN_NAMESPACE

class FieldInfo
{
public:
    FieldInfo(const Island::FieldData & data) : m_data(data) {}

    inline bool hasBuilding() const { return m_data.hasBuilding(); }
    inline bool hasRoad() const { return m_data.hasRoad(); }
    inline qreal roadSpeedFactor() const { return RoadLibrary::prototype(m_data.roadType)->speedFactor; }

private:
    const Island::FieldData & m_data;
};

struct RoutingNeighbor
{
    RoutingNeighbor() { qFatal("Constructor RoutingNeighbor() should never be called."); }
    RoutingNeighbor(QPoint coordinates, qreal speedFactor = 1.0) :
        coordinates(coordinates), speedFactor(speedFactor)
    {}

    QPoint coordinates;
    qreal speedFactor;
};

class RoutingParameters
{
public:
    inline QPoint currentCoordinates() const { return m_pos; }

    inline QPoint coordinatesRelative(QPoint offset) const { return m_pos + offset; }
    inline QPoint coordinatesRelative(int dx, int dy) const { return QPoint(m_pos.x() + dx, m_pos.y() + dy); }

    inline bool legalCoordinates(QPoint coordinates) const { return m_island->checkBounds(coordinates); }
    inline bool legalCoordinates(int x, int y) const { return m_island->checkBounds(x, y); }

    inline bool legalCoordinatesRelative(QPoint offset) const { return m_island->checkBounds(m_pos + offset); }
    inline bool legalCoordinatesRelative(int dx, int dy) const { return m_island->checkBounds(m_pos.x() + dx, m_pos.y() + dy); }

    inline FieldInfo currentField() const { return fieldAt(m_pos); }

    inline FieldInfo fieldAt(QPoint coordinates) const { Q_ASSERT(legalCoordinates(coordinates)); return FieldInfo(m_island->fieldAt(coordinates)); }
    inline FieldInfo fieldAt(int x, int y) const { Q_ASSERT(legalCoordinates(x, y)); return FieldInfo(m_island->fieldAt(x, y)); }

    inline FieldInfo fieldRelative(QPoint offset) const { Q_ASSERT(legalCoordinatesRelative(offset)); return FieldInfo(m_island->fieldAt(m_pos + offset)); }
    inline FieldInfo fieldRelative(int dx, int dy) const { Q_ASSERT(legalCoordinatesRelative(dx, dy)); return FieldInfo(m_island->fieldAt(m_pos.x() + dx, m_pos.y() + dy)); }

libprivate:
    RoutingParameters();

    const Island *m_island;
    QPoint m_pos;
    void *m_internalData;
};

OAE_END_NAMESPACE

#endif // ROUTINGPARAMETERS_H
