#include "scheduler.h"
#include "scheduled.h"

Scheduler::Scheduler(QObject *parent) :
    QObject(parent),
    queueOffset(-1)
{
}

void Scheduler::insert(Scheduled *object, Time timeout)
{
    while(timeout > queue.length())
        queue.insert(++queueOffset, QList<Scheduled*>());

    queue[(queueOffset + timeout) % queue.length()].append(object);
}

void Scheduler::iterate()
{
    QList<Scheduled*> objects = queue[queueOffset];
    queue[queueOffset].clear();

    foreach(Scheduled *object, objects)
        object->_schedule();

    queueOffset = (queueOffset + 1) % queue.length();
}
