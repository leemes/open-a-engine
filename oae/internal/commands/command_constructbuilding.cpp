#include "../../command.h"
#include "../../player.h"
#include "../../island.h"
#include "../../town.h"
#include "../../building.h"
#include "../libraries/buildinglibrary.h"

Command::Error Command::checkConstructBuilding() const
{
    verifyArgCount(3);
    Player *player = subj<Player>();
    Town *town = obj<Town>();
    uint buildingProtoID = arg<uint>(0);
    QPoint pos = arg<QPoint>(1);
    Orientation orientation = (Orientation)arg<int>(2);

    if(town->player() != player)
        return CommandNotAllowed;
    BuildingPrototype *proto = BuildingLibrary::prototype(buildingProtoID);
    if(!town->checkGoodsInStorage(proto->constructionCosts.localGoods()))
        return CommandNotAllowed;
    if(!player->checkGlobalGoods(proto->constructionCosts.globalGoods()))
        return CommandNotAllowed;
    return NoError;
}

void Command::executeConstructBuilding()
{
    Town *town = obj<Town>();
    uint buildingProtoID = arg<uint>(0);
    QPoint pos = arg<QPoint>(1);
    Orientation orientation = (Orientation)arg<int>(2);

    town->newBuilding(pos, orientation, buildingProtoID);
}
