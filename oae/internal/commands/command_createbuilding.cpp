#include "command_createbuilding.h"
#include "../../libraries/buildinglibrary.h"
#include "../../prototypes/buildingprototype.h"
#include "../../town.h"
#include "../../island.h"
#include "../../player.h"

Command::Error Command_CreateBuilding::check() const
{
    Player *player = town->player();

    BuildingPrototype *proto = BuildingLibrary::prototype(protoID);
    Goods costs = proto->constructionCosts;

    if(!town->checkGoodsInStorage(costs.localGoods()))
        return Command::NotEnoughLocalResources;
    if(!player->checkGlobalGoods(costs.globalGoods()))
        return Command::NotEnoughGlobalResources;

    Island::BuildabilityStatus buildability = town->island()->checkBuildability(player->id(), proto, position, orientation);
    switch(buildability) {
    case Island::NotBuildableNotOnIsland:
        return Command::NotBuildableNotOnIsland;
    case Island::NotBuildableTerrain:
        return Command::NotBuildableTerrain;
    case Island::NotBuildableCollision:
        return Command::NotBuildableCollision;
    case Island::NotBuildableLicense:
        return Command::NotBuildableLicense;
    case Island::Buildable:
        return Command::NoError;
    }
    return Command::NoError;
}

void Command_CreateBuilding::execute() const
{
    Player *player = town->player();

    BuildingPrototype *proto = BuildingLibrary::prototype(protoID);
    Goods costs = proto->constructionCosts;

    town->takeGoodsFromStorage(costs.localGoods());
    player->takeGlobalGoods(costs.globalGoods());

    town->newBuilding(position, orientation, protoID);
}
