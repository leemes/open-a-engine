#include "command_createroad.h"
#include "../../town.h"
#include "../../island.h"
#include "../../player.h"
#include "../../libraries/roadlibrary.h"
#include "../../prototypes/roadprototype.h"

// TODO: Avoid redundant code!

Command::Error Command_CreateRoad::check() const
{
    Player *player = town->player();
    Island *island = town->island();
    RoadPrototype *proto = RoadLibrary::prototype(protoID);

    int count = 0;
    foreach(QPoint pos, fields)
        if(island->checkBounds(pos) && island->road(pos) != protoID)
            count++;

    if(count > 0) {
        Goods costs = (proto->constructionCosts * count);
        costs.divideRoundedUp(proto->constructionCostsDivisor);
        if(!town->checkGoodsInStorage(costs.localGoods()))
            return Command::NotEnoughLocalResources;
        if(!player->checkGlobalGoods(costs.globalGoods()))
            return Command::NotEnoughGlobalResources;
    }

    foreach(QPoint pos, fields)
    {
        if(island->checkBounds(pos))
        {
            Island::BuildabilityStatus buildability = island->checkBuildability(player->id(), proto, pos);

            switch(buildability) {
            case Island::NotBuildableTerrain:
                return Command::NotBuildableTerrain;
            case Island::NotBuildableNotOnIsland:
                return Command::NotBuildableNotOnIsland;
            case Island::NotBuildableCollision:
                return Command::NotBuildableCollision;
            case Island::NotBuildableLicense:
                return Command::NotBuildableLicense;
            case Island::Buildable:
                break;
            }
        }
        else
        {
            qWarning("Road path out of island bounds");
            return Command::NotBuildableNotOnIsland;
        }
    }

    return Command::NoError;
}

void Command_CreateRoad::execute() const
{
    Player *player = town->player();
    Island *island = town->island();
    RoadPrototype *proto = RoadLibrary::prototype(protoID);

    int count = 0;
    foreach(QPoint pos, fields) {
        if(island->checkBounds(pos) && island->road(pos) != protoID) {
            count++;
            island->setRoad(pos, protoID);
        }
    }

    if(count > 0) {
        Goods costs = (proto->constructionCosts * count);
        costs.divideRoundedUp(proto->constructionCostsDivisor);
        town->takeGoodsFromStorage(costs.localGoods());
        player->takeGlobalGoods(costs.globalGoods());
    }
}
