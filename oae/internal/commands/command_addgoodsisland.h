#ifndef COMMAND_ADDGOODSISLAND_H
#define COMMAND_ADDGOODSISLAND_H

#include "../../global.h"
#include "abstractcommand.h"
#include "../../goods.h"

OAE_BEGIN_NAMESPACE

class Town;

class Command_AddGoodsIsland : public AbstractCommand
{
public:
    Command_AddGoodsIsland(Town *town, LocalGoods goods) :
        AbstractCommand(AddGoodsIsland),
        town(town), goods(goods)
    {}

    Command::Error check() const;
    void execute() const;

private:
    Town *town;
    LocalGoods goods;
};

OAE_END_NAMESPACE

#endif // COMMAND_ADDGOODSISLAND_H
