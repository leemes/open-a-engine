#include "../../command.h"
#include "../../player.h"
#include "../../island.h"
#include "../../town.h"
#include "../libraries/roadlibrary.h"

Command::Error Command::checkConstructRoad() const
{
    verifyArgCount(2);
    Player *player = subj<Player>();
    Town *town = obj<Town>();
    uint roadProtoID = arg<uint>(0);
    QPoint pos = arg<QPoint>(1);

    if(town->player() != player)
        return CommandNotAllowed;
    RoadPrototype *proto = RoadLibrary::prototype(roadProtoID);
    if(!town->checkGoodsInStorage(proto->constructionCosts.localGoods()))
        return CommandNotAllowed;
    if(!player->checkGlobalGoods(proto->constructionCosts.globalGoods()))
        return CommandNotAllowed;
    return NoError;
}

void Command::executeConstructRoad()
{
    Town *town = obj<Town>();
    uint roadProtoID = arg<uint>(0);
    QPoint pos = arg<QPoint>(1);

    Island *island = town->island();
    island->setRoad(pos, roadProtoID);
}
