#ifndef COMMAND_CREATEROAD_H
#define COMMAND_CREATEROAD_H

#include "../../global.h"
#include "../../namespace.h"
#include "abstractcommand.h"
#include <QPoint>

OAE_BEGIN_NAMESPACE

class Town;

class Command_CreateRoad : public AbstractCommand
{
public:
    Command_CreateRoad(Town *town, uint protoID, QList<QPoint> fields) :
        AbstractCommand(CreateRoad),
        town(town), protoID(protoID), fields(fields)
    {}

    Command::Error check() const;
    void execute() const;

private:
    Town *town;
    uint protoID;
    QList<QPoint> fields;
};

OAE_END_NAMESPACE

#endif // COMMAND_CREATEROAD_H
