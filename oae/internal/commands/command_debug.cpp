#include "command_debug.h"
#include "../../town.h"
#include "../../building.h"
#include "../../transportation.h"
#include <QDebug>


Command::Error Command_Debug::check() const
{
    return Command::NoError;
}

void Command_Debug::execute() const
{
    GoodID good = 4; // wood

    Building *warehouse, *lumberjack;
    foreach(Building *b, town->buildings()) {
        if(b->typeName() == "small warehouse")
            warehouse = b;
        if(b->typeName() == "lumberjacks hut")
            lumberjack = b;
    }
    Q_ASSERT(warehouse && lumberjack);

    Transportation *transportationUnit = warehouse->transportationUnits.first();
    Q_ASSERT(transportationUnit);

    qDebug("Transportation unit info:");
    qDebug() << transportationUnit->m_proto << transportationUnit->m_goods << transportationUnit->m_collect
                << transportationUnit->m_client << transportationUnit->m_state << transportationUnit->m_location
                   << transportationUnit->m_path.time() << transportationUnit->m_lastStateChange;

    Building::ConnectedBuildingsPaths buildingsPaths = warehouse->targetBuildings(transportationUnit);
    LocalGoods collectGoods;
    collectGoods[good] = lumberjack->m_cs.production.outputFill;

    transportationUnit->startJob(lumberjack, buildingsPaths[lumberjack], LocalGoods(), collectGoods);
}
