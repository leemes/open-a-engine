#include "command_deleteroad.h"
#include "../../town.h"
#include "../../island.h"


Command::Error Command_DeleteRoad::check() const
{
    return Command::NoError;
}

void Command_DeleteRoad::execute() const
{
    Island *island = town->island();

    foreach(QPoint pos, fields)
        if(island->road(pos) != 0)
            island->setRoad(pos, 0);
}
