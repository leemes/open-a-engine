#include "../../command.h"
#include "../../player.h"
#include "../../island.h"
#include "../../goods.h"
#include "../../town.h"
#include "../libraries/buildinglibrary.h"
#include "../prototype/buildingprototype.h"

Command::Error Command::checkCreateTown() const
{
    verifyArgCount(5);
    Player *player = subj<Player>();
    Island *island = obj<Island>();
    QString name = arg<QString>(0);
    LocalGoods initialGoods = arg<LocalGoods>(1);
    uint buildingProtoID = arg<uint>(2);
    QPoint pos = arg<QPoint>(3);
    Orientation orientation = (Orientation)arg<int>(4);

    BuildingPrototype *proto = BuildingLibrary::prototype(buildingProtoID);
    foreachkv(GoodID good, int requiredAmount, proto->constructionCosts.localGoods())
        if(initialGoods.value(good, 0) < requiredAmount)
            return CommandNotAllowed;
    if(!player->checkGlobalGoods(proto->constructionCosts.globalGoods()))
        return CommandNotAllowed;
    return NoError;
}

void Command::executeCreateTown()
{
    Player *player = subj<Player>();
    Island *island = obj<Island>();
    QString name = arg<QString>(0);
    LocalGoods initialGoods = arg<LocalGoods>(1);
    uint buildingProtoID = arg<uint>(2);
    QPoint pos = arg<QPoint>(3);
    Orientation orientation = (Orientation)arg<int>(4);

    Town *town = player->newTown(island, name);
    // construct the building and substract the construction goods from the initialGoods
    town->newBuilding(pos, orientation, buildingProtoID);
    BuildingPrototype *proto = BuildingLibrary::prototype(buildingProtoID);
    foreachkv(GoodID good, int requiredAmount, proto->constructionCosts.localGoods())
        initialGoods[good] -= requiredAmount;
    // assert that the initialGoods contained at least the goods needed to construct the building
    foreach(int remainingAmount, initialGoods)
        Q_ASSERT(remainingAmount >= 0);
    // the remaining goods are added to the town's storage
    town->addGoodsToStorage(initialGoods);
    player->takeGlobalGoods(proto->constructionCosts.globalGoods());
}

