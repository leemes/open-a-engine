#include "abstractcommand.h"

AbstractCommand::AbstractCommand(AbstractCommand::Type type) :
    m_type(type)
{
}

AbstractCommand::Type AbstractCommand::type() const
{
    return m_type;
}
