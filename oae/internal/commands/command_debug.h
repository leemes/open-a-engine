#ifndef COMMAND_DEBUG_H
#define COMMAND_DEBUG_H

#include "../../global.h"
#include "../../namespace.h"
#include "abstractcommand.h"
#include <QPoint>

OAE_BEGIN_NAMESPACE

class Town;

class Command_Debug : public AbstractCommand
{
public:
    Command_Debug(Town *town) :
        AbstractCommand(Debug),
        town(town)
    {}

    Command::Error check() const;
    void execute() const;

private:
    Town *town;
};

OAE_END_NAMESPACE

#endif // COMMAND_DEBUG_H
