#ifndef ABSTRACTCOMMAND_H
#define ABSTRACTCOMMAND_H

#include "../../global.h"
#include "../../command.h"

OAE_BEGIN_NAMESPACE

class AbstractCommand
{
public:
    enum Type {
        Debug,
        AddGoodsIsland,
        CreateBuilding,
        CreateRoad,
        DeleteRoad,

        TypeCount
    };

    explicit AbstractCommand(Type type);
    virtual inline ~AbstractCommand() {}

    Type type() const;

    virtual Command::Error check() const = 0;
    virtual void execute() const = 0;

private:
    Type m_type;
};

OAE_END_NAMESPACE

#endif // ABSTRACTCOMMAND_H
