#ifndef COMMAND_DELETEROAD_H
#define COMMAND_DELETEROAD_H

#include "../../global.h"
#include "../../namespace.h"
#include "abstractcommand.h"
#include <QPoint>

OAE_BEGIN_NAMESPACE

class Town;

class Command_DeleteRoad : public AbstractCommand
{
public:
    Command_DeleteRoad(Town *town, QList<QPoint> fields) :
        AbstractCommand(DeleteRoad),
        town(town), fields(fields)
    {}

    Command::Error check() const;
    void execute() const;

private:
    Town *town;
    QList<QPoint> fields;
};

OAE_END_NAMESPACE

#endif // COMMAND_DELETEROAD_H
