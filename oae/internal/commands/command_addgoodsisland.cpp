#include "command_addgoodsisland.h"
#include "../../town.h"

Command::Error Command_AddGoodsIsland::check() const
{
    return Command::NoError;
}

void Command_AddGoodsIsland::execute() const
{
    town->addGoodsToStorage(goods);
}
