#ifndef COMMAND_CREATEBUILDING_H
#define COMMAND_CREATEBUILDING_H

#include "../../global.h"
#include "../../namespace.h"
#include "abstractcommand.h"
#include <QPoint>

OAE_BEGIN_NAMESPACE

class Town;

class Command_CreateBuilding : public AbstractCommand
{
public:
    Command_CreateBuilding(Town *town, uint protoID, QPoint position, Orientation orientation) :
        AbstractCommand(CreateBuilding),
        town(town), protoID(protoID), position(position), orientation(orientation)
    {}

    Command::Error check() const;
    void execute() const;

private:
    Town *town;
    uint protoID;
    QPoint position;
    Orientation orientation;
};

OAE_END_NAMESPACE

#endif // COMMAND_CREATEBUILDING_H
