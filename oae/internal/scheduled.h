#ifndef SCHEDULED_H
#define SCHEDULED_H

#include "../global.h"
#include "../namespace.h"

OAE_BEGIN_NAMESPACE

class Game;

/** An abstract class providing extensions for other classes to make use of the scheduling mechanism.
 * It also provides (and enforces) delayed destruction, meaning that you can never delete an instance
 * directly, but tell it to be deleted after a specific period of time (specified by the Game instance
 * provided in the constructor).
 */
class Scheduled
{
    friend class Scheduler; // calls _schedule()

public:
    Scheduled(Game *game, bool initialSchedule = true, Time initialTimeout = 1);
    ~Scheduled();

    void markForDeletion();
    bool markedForDeletion() const;
    bool stillExists() const;

protected:
    void reschedule(Time timeout);
    Time gameTime() const;
    TimeF gameTimeF() const;

private:
    // Called by the Scheduler; calls schedule() as long as this object still exists
    void _schedule();

    // Called after the initial or reschedule timeout has been expired.
    // Reimplemented in the subclass.
    virtual void schedule() = 0;

    // Called when the object has been marked for deletion using markForDeletion().
    // Optionally reimplemented in the subclass.
    virtual void hasBeenMarkedForDeletion() {}

    // Called just before the object gets deleted in memory.
    // Reimplemented in the subclass.
    virtual void deleteNow() = 0;

private:
    Game *m_game; // needed to ask for the scheduler and the deletion delay
    bool m_mark; // set when markForDeletion() gets called
    Time m_deleteTime; // time of death
    bool m_delete; // set when the deletion delay has been expired, just before deleteNow() gets called
};

OAE_END_NAMESPACE

#endif // SCHEDULED_H
