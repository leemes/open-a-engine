#ifndef SCHEDULER_H
#define SCHEDULER_H

#include "../global.h"
#include "../namespace.h"
#include <QObject>

OAE_BEGIN_NAMESPACE

class Scheduled;

class Scheduler : public QObject
{
    Q_OBJECT
public:
    explicit Scheduler(QObject *parent = 0);

    void insert(Scheduled *object, Time timeout);

public slots:
    void iterate();

private:
    QList<QList<Scheduled*> > queue;
    int queueOffset;
};

OAE_END_NAMESPACE

#endif // SCHEDULER_H
