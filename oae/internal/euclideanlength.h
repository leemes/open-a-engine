#ifndef EUCLIDEANLENGTH_H
#define EUCLIDEANLENGTH_H

#include <cmath>
#include <QtGlobal>

template<class POINT>
inline qreal euclideanLength(POINT offset) {
    return sqrt(offset.x() * offset.x() + offset.y() * offset.y());
}

inline qreal euclideanLength(qreal x, qreal y) {
    return sqrt(x * x + y * y);
}

#endif // EUCLIDEANLENGTH_H
