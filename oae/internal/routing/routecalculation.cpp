#include "routecalculation.h"
#include "../hashfunctions.h"
#include "../euclideanlength.h"


RouteCalculation::RouteCalculation(const Island *island, RoutingConditionFunc routingConditionFunction,
                                   RoutingStrategy strategy, qreal baseSpeed, QPoint source, QList<QPoint> targets) :
    m_island(island),
    m_routingConditionFunction(routingConditionFunction),
    m_routingConditionInternalData(0),
    m_strategy(strategy),
    m_baseSpeed(baseSpeed),
    m_source(source),
    m_targets(targets.toSet()), // we need it as a set
    m_found(false)
{
    setAutoDelete(false);
}

void RouteCalculation::setRoutingConditionInternalData(void *internalData)
{
    m_routingConditionInternalData = internalData;
}

void RouteCalculation::run()
{
    m_found = false;

    // TODO: Improve route calculation: implement A* and / or use a better priority queue
    QHash<QPoint,qreal> openCosts;             // <-- queue
    QHash<QPoint,RoutingNeighbor> openEdges;   // reverse edges (i.e. the .coordinates is the parent point)
    QHash<QPoint,RoutingNeighbor> closedEdges; // reverse edges (i.e. the .coordinates is the parent point)

    openCosts.insert(m_source, 0.0);
    openEdges.insert(m_source, RoutingNeighbor(m_source));

    // re-usable routing parameters
    RoutingParameters params;
    params.m_island = m_island;
    params.m_pos = QPoint(); //<-- we will use this as our "current position" too, so we don't need another variable for it.
    params.m_internalData = m_routingConditionInternalData;

    while(openCosts.count())
    {
        // find minimum (possible improvement)
        qreal currentCosts = 1e100; // some platforms might not have numeric_limits<double>::infinity()...
        foreachkv(QPoint p, qreal c, openCosts) {
            if(c < currentCosts) {
                currentCosts = c;
                params.m_pos = p;
            }
        }

        // end condition: one of the targets reached
        if(m_targets.contains(params.m_pos))
        {
            m_found = true;
            closedEdges.insert(params.m_pos, openEdges.take(params.m_pos));
            break;
        }

        // find neighbors
        QList<RoutingNeighbor> neighbors = (*m_routingConditionFunction)(&params);

        // add neighbors
        foreach(const RoutingNeighbor & neighbor, neighbors) {
            if(!closedEdges.contains(neighbor.coordinates)) {
                qreal edgeCosts = getCosts(params.m_pos, neighbor);
                qreal newCosts = currentCosts + edgeCosts;
                // did we reduce the costs?
                if(!openCosts.contains(neighbor.coordinates) || newCosts < openCosts[neighbor.coordinates]) {
                    openCosts[neighbor.coordinates] = newCosts;
                    openEdges.insert(neighbor.coordinates, RoutingNeighbor(params.m_pos, neighbor.speedFactor));
                }
            }
        }

        // close this node
        closedEdges.insert(params.m_pos, openEdges.take(params.m_pos));
        openCosts.take(params.m_pos);
    }

    if(m_found)
    {
        QList<PathSegment> edges;
        QPoint p = params.m_pos; // start with the last point we've found
        if(p == m_source) {
            // insert a single empty PathSegment since Path requires at least one PathSegment
            PathSegment edge;
            edge.from = p;
            edge.to = p;
            edge.speed = m_baseSpeed; // irrelevant
        } else {
            while(p != m_source) {
                PathSegment edge;
                edge.to = p;
                edge.from = closedEdges[p].coordinates;
                edge.speed = m_baseSpeed * closedEdges[p].speedFactor;
                edges.prepend(edge);
                p = edge.from;
            }
        }
        m_path.segments = edges;
        m_path.precompute();
    }
}

qreal RouteCalculation::getCosts(const QPoint & currentCoordinates, const RoutingNeighbor &n) const
{
    if(m_strategy == ShortestPath)
        return euclideanLength(n.coordinates - currentCoordinates);
    else
        return euclideanLength(n.coordinates - currentCoordinates) / n.speedFactor;
}


bool RouteCalculation::routeFound() const
{
    return m_found;
}

Path RouteCalculation::bestRoute() const
{
    return m_path;
}
