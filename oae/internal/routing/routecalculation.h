#ifndef ROUTECALCULATION_H
#define ROUTECALCULATION_H

#include "../../global.h"
#include <QRunnable>
#include <QPoint>
#include <QSet>
#include "routingconditionfunctions.h"
#include "../../path.h"

OAE_BEGIN_NAMESPACE

class RouteCalculation : public QRunnable
{
public:

    RouteCalculation(const Island *island, RoutingConditionFunc routingConditionFunction, RoutingStrategy strategy,
                     qreal baseSpeed, QPoint source, QList<QPoint> targets);

    void setRoutingConditionInternalData(void *internalData);

    virtual void run();

    bool routeFound() const;
    Path bestRoute() const;

private:
    const Island *m_island;
    RoutingConditionFunc m_routingConditionFunction;
    void *m_routingConditionInternalData;
    RoutingStrategy m_strategy;
    qreal m_baseSpeed;
    QPoint m_source;
    QSet<QPoint> m_targets;
    bool m_found;
    Path m_path;
    inline qreal getCosts(const QPoint &currentCoordinates, const RoutingNeighbor &) const;
};

OAE_END_NAMESPACE

#endif // ROUTECALCULATION_H
