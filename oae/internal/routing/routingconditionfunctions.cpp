#include "routingconditionfunctions.h"
#include "../../error.h"
#include "../../game.h"


QList<RoutingNeighbor> routingFuncRoadBuildability(RoutingParameters *routing)
{
    RoadPrototype *proto = reinterpret_cast<RoadPrototype*>(routing->m_internalData);
    const Island *island = routing->m_island;
    PlayerID playerId = island->game()->thisPlayerId();

    QList<RoutingNeighbor> neighbors;
    QList<QPoint> offsets; offsets << QPoint(1, 0) << QPoint(-1, 0) << QPoint(0, 1) << QPoint(0, -1);

    foreach(QPoint offset, offsets)
    {
        if(routing->legalCoordinatesRelative(offset) && island->checkBuildability(playerId, proto, routing->coordinatesRelative(offset)) == Island::Buildable)
        {
            // prefer horizontal edges over vertical edges
            qreal speedFactor = (offset.y() == 0) ? 1001 : 1000;
            neighbors << RoutingNeighbor(routing->coordinatesRelative(offset), speedFactor);
        }
    }

    return neighbors;
}

QList<RoutingNeighbor> routingFuncAnywhereOrthogonal(RoutingParameters *routing)
{
    QList<RoutingNeighbor> neighbors;
    QList<QPoint> offsets; offsets << QPoint(1, 0) << QPoint(-1, 0) << QPoint(0, 1) << QPoint(0, -1);

    foreach(QPoint offset, offsets)
        if(routing->legalCoordinatesRelative(offset))
            neighbors << routing->coordinatesRelative(offset);

    return neighbors;
}

QList<RoutingNeighbor> routingFuncRoadOrthogonal(RoutingParameters *routing)
{
    qreal sourceSpeedFactor = routing->currentField().roadSpeedFactor();
    QList<RoutingNeighbor> neighbors;
    QList<QPoint> offsets; offsets << QPoint(1, 0) << QPoint(-1, 0) << QPoint(0, 1) << QPoint(0, -1);

    foreach(QPoint offset, offsets)
    {
        if(routing->legalCoordinatesRelative(offset) && routing->fieldRelative(offset).hasRoad())
        {
            qreal targetSpeedFactor = routing->fieldRelative(offset).roadSpeedFactor();
            qreal speedFactor = (sourceSpeedFactor + targetSpeedFactor) / 2;
            neighbors << RoutingNeighbor(routing->coordinatesRelative(offset), speedFactor);
       }
    }

    return neighbors;
}



RoutingConditionFunc getRoutingConditionFunctionForName(QString name)
{
    if(name == "road orthogonal")
        return routingFuncRoadOrthogonal;
    if(name == "anywhere orthogonal")
        return routingFuncAnywhereOrthogonal;
    if(name == "road buildability")
        return routingFuncRoadBuildability;

    Error::report(0, Error::RulesParseError, "No such routing condition function defined", name);
    return 0;
}
