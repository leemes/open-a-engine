#ifndef ROUTINGCONDITIONFUNCTIONS_H
#define ROUTINGCONDITIONFUNCTIONS_H

#include "../../global.h"
#include "../../namespace.h"
#include "../../routingparameters.h"

class QPoint;

OAE_BEGIN_NAMESPACE

typedef QList<RoutingNeighbor> (*RoutingConditionFunc)(RoutingParameters*);

RoutingConditionFunc getRoutingConditionFunctionForName(QString name);

OAE_END_NAMESPACE

#endif // ROUTINGCONDITIONFUNCTIONS_H
