#include "sparsebitarray2d.h"

SparseBitArray2D::SparseBitArray2D(QSize size) :
    m_size(size)
{
}

bool SparseBitArray2D::bitAt(QPoint p) const
{
    int index = blockIndex(p);
    if(m_blocks.contains(index))
        return m_blocks.value(index).bitAt(blockSubCoord(p));
    else
        return false;
}

void SparseBitArray2D::setBitAt(QPoint p, bool value)
{
    int index = blockIndex(p);
    m_blocks[index].setBitAt(blockSubCoord(p));
    Q_ASSERT(bitAt(p) == value);
}


SparseBitArray2D::Block::Block()
{
    memset(this, 0, sizeof(*this));
}

bool SparseBitArray2D::Block::bitAt(QPoint subCoord) const
{
    quint32 mask = 1 << subCoord.x();
    return data[subCoord.y()] & mask;
}

void SparseBitArray2D::Block::setBitAt(QPoint subCoord, bool value)
{
    quint32 mask = 1 << subCoord.x();
    data[subCoord.y()] = (data[subCoord.y()] & ~mask) | ((quint32)value << subCoord.x());
    Q_ASSERT(bitAt(subCoord) == value);
}
