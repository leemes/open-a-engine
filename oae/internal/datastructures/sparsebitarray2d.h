#ifndef SPARSEBITARRAY2D_H
#define SPARSEBITARRAY2D_H

#include "../../global.h"
#include <QPoint>
#include <QMap>
#include <QSize>

OAE_BEGIN_NAMESPACE

/**
 * @brief A compact representation of a sparse bit field indexed by points in 2D space.
 *
 * Initially all fields have a value of false. The space is divided into 32 x 32 blocks, for which data gets allocated
 * only when needed (128 bytes per block). Data never gets deallocated for blocks becoming empty, only at destruction
 * of the whole array.
 */
class SparseBitArray2D
{
public:
    SparseBitArray2D(QSize size);

    bool bitAt(QPoint p) const;
    void setBitAt(QPoint p, bool value = true);

private:
    struct Block {
        Block();
        quint32 data[32];
        bool bitAt(QPoint subCoord) const;
        void setBitAt(QPoint subCoord, bool value = true);
    };

    inline uint blockIndex(QPoint point) const {
        return (point.x() / 32) + (point.y() / 32) * (m_size.height() + 31) / 32;
    }
    inline QPoint blockSubCoord(QPoint point) const {
        return QPoint(point.x() % 32, point.y() % 32);
    }

    QSize m_size;
    QMap<uint,Block> m_blocks;
};

OAE_END_NAMESPACE

#endif // SPARSEBITARRAY2D_H
