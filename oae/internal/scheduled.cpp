#include "scheduled.h"
#include "scheduler.h"
#include "../game.h"

Scheduled::Scheduled(Game *game, bool initialSchedule, Time initialTimeout) :
    m_game(game),
    m_mark(false),
    m_delete(false)
{
    if(initialSchedule)
        reschedule(initialTimeout);
}

Scheduled::~Scheduled()
{
    if(!m_delete)
        qFatal("Scheduled object deleted manually. Call markForDeletion() intead!");
}

void Scheduled::markForDeletion()
{
    if(m_mark)
        qFatal("Scheduled object marked twice for deletion!");
    else {
        m_mark = true;
        Time delay = timeFromSeconds(60); // TODO: dynamic deletion delay!
        m_deleteTime = m_game->time() + delay;
        hasBeenMarkedForDeletion();
        reschedule(delay);
    }
}

bool Scheduled::markedForDeletion() const
{
    return m_mark;
}

bool Scheduled::stillExists() const
{
    return !m_mark;
}

void Scheduled::reschedule(Time timeout)
{
    if(timeout > 0)
        m_game->scheduler()->insert(this, timeout);
    else
        schedule();
}

Time Scheduled::gameTime() const
{
    return m_game->time();
}

TimeF Scheduled::gameTimeF() const
{
    return m_game->timeF();
}

void Scheduled::_schedule()
{
    if(!m_mark)
        schedule();
    else if(m_game->time() >= m_deleteTime) {
        m_delete = true;
        deleteNow();
    }
}

