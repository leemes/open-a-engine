#ifndef QTHASHFUNCTIONS_H
#define QTHASHFUNCTIONS_H

#include <QHash>
#include <QPoint>

inline uint qHash(QPoint point) {
    return qHash(qMakePair(point.x(), point.y()));
}

#endif // QTHASHFUNCTIONS_H
