#include "town.h"
#include "player.h"
#include "island.h"
#include "building.h"
#include "game.h"
#include "libraries/buildinglibrary.h"


Town::Town(Player *player, Island *island, QString name) :
    QObject(player),
    Scheduled(island->game()),
    m_island(island),
    m_name(name),
    m_totalStorageCapacity(0)
{
    m_island->registerNewTown(this);
}

Town::~Town()
{
    m_island->registerDeleteTown(this);
}

Player *Town::player() const
{
    return qobject_cast<Player*>(parent());
}

Island *Town::island() const
{
    return m_island;
}

World *Town::world() const
{
    return island()->world();
}

Game *Town::game() const
{
    return island()->game();
}

Building *Town::newBuilding(QPoint position, Orientation orientation, uint protoID)
{
    BuildingPrototype *proto = BuildingLibrary::prototype(protoID);
    Building *building = new Building(island(), this, proto, position, orientation);

    m_buildings << building;
    if(building->isTradeBuilding())
        updateTotalStorageCapacity();

    emit reportNewBuilding(building);
    return building;
}

void Town::deleteBuilding(Building *building)
{
    //building->markForDeletion();

    m_buildings.removeOne(building);
    if(building->isTradeBuilding())
        updateTotalStorageCapacity();

    emit reportDeleteBuilding(building);
}

QString Town::name() const
{
    return m_name;
}

QList<Building *> Town::buildings() const
{
    return m_buildings;
}

int Town::totalStorageCapacity() const
{
    return m_totalStorageCapacity;
}

const LocalGoods &Town::currentStorage() const
{
    return m_storage;
}

int Town::currentStorage(GoodID good) const
{
    return m_storage[good];
}

void Town::addGoodsToStorage(const LocalGoods & goods)
{
    foreachkv(GoodID good, int amount, goods)
        addGoodsToStorage(good, amount);
}

void Town::addGoodsToStorage(GoodID good, int amount)
{
    m_storage[good] += amount;
    if(m_storage[good] > m_totalStorageCapacity)
        m_storage[good] = m_totalStorageCapacity;
}

bool Town::checkGoodsInStorage(const LocalGoods & goods) const
{
    foreachkv(GoodID good, int amount, goods)
        if(!checkGoodsInStorage(good, amount))
            return false;
    return true;
}

bool Town::checkGoodsInStorage(GoodID good, int amount) const
{
    return m_storage[good] >= amount;
}

bool Town::takeGoodsFromStorage(const LocalGoods & goods)
{
    if(!checkGoodsInStorage(goods))
        return false;
    foreachkv(GoodID good, int amount, goods)
        m_storage[good] -= amount;
    return true;
}

bool Town::takeGoodsFromStorage(GoodID good, int amount)
{
    if(!checkGoodsInStorage(good, amount))
        return false;
    m_storage[good] -= amount;
    return true;
}

int Town::currentStorageIncludingReservedIncoming(GoodID good) const
{
    return m_storage[good] + m_reservedIncoming[good];
}

void Town::reserveIncoming(GoodID good, int amount)
{
    m_reservedIncoming[good] += amount;
}

int Town::reservedIncoming(GoodID good)
{
    return m_reservedIncoming[good];
}

void Town::addReservedIncoming(GoodID good, int amount)
{
    Q_ASSERT(m_reservedIncoming[good] >= amount);
    addGoodsToStorage(good, amount);
    m_reservedIncoming[good] -= amount;
}

int Town::currentStorageExcludingReservedOutgoing(GoodID good) const
{
    return qMax(0, m_storage[good] - m_reservedOutgoing[good]);
}

void Town::reserveOutgoing(GoodID good, int amount)
{
    Q_ASSERT(m_storage[good] - m_reservedOutgoing[good] >= amount);
    m_reservedOutgoing[good] += amount;
}

int Town::reservedOutgoing(GoodID good)
{
    return m_reservedOutgoing[good];
}

int Town::takeReservedOutgoing(GoodID good, int amount)
{
    Q_ASSERT(m_reservedOutgoing[good] >= amount);
    m_reservedOutgoing[good] -= amount;
    int actualAmount = qMin(amount, currentStorage(good));
    bool success = takeGoodsFromStorage(good, amount);
    Q_ASSERT(success);
    return actualAmount;
}

void Town::schedule()
{
    updateTotalStorageCapacity();

    reschedule(10);
}

void Town::deleteNow()
{
    delete this;
}

void Town::updateTotalStorageCapacity()
{
    int newCapacity = 0;
    foreach(Building *building, m_buildings)
        if(building->isTradeBuilding())
            newCapacity += building->storageCapacity();
    newCapacity = qMin(newCapacity, (int)Limits::maxStorageCapacity);
    if(newCapacity != m_totalStorageCapacity)
        qDebug("Storage capacity of %s changed from %d to %d", qPrintable(name()), m_totalStorageCapacity, newCapacity);
    m_totalStorageCapacity = newCapacity;
    foreachkv(GoodID good, int amount, m_storage)
        if(amount > m_totalStorageCapacity)
            m_storage[good] = m_totalStorageCapacity;
}

