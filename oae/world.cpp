#include "world.h"
#include "game.h"
#include "island.h"


World::World(Game *game, QSize size) :
    QObject(game),
    m_size(size)
{
}

Game *World::game() const
{
    return qobject_cast<Game*>(parent());
}

QSize World::size() const
{
    return m_size;
}

int World::width() const
{
    return m_size.width();
}

int World::height() const
{
    return m_size.height();
}

Island *World::newIsland(QRect boundary)
{
    Island *island = new Island(this, boundary);
    m_islands << island;
    connect(island, SIGNAL(reportNewBuilding(Building*)),
            this, SIGNAL(reportNewBuilding(Building*)));
    connect(island, SIGNAL(reportRoadChanged(Island*,QPoint,uint)),
            this, SIGNAL(reportRoadChanged(Island*,QPoint,uint)));
    return island;
}

QList<Island *> World::islands() const
{
    return m_islands;
}

Island *World::islandAt(QPoint pos) const
{
    foreach(Island *island, m_islands)
        if(island->boundaries().contains(pos))
            return island;
    return 0;
}
