#ifndef ISLAND_H
#define ISLAND_H

#include "global.h"
#include "namespace.h"
#include "limits.h"
#include <QObject>
#include <QRect>
#include <QList>
#include <QMap>
#include <QSet>

OAE_BEGIN_NAMESPACE

class World;
class Game;
class Town;
class Building;
class BuildingPrototype;
class RoadPrototype;
class SparseBitArray2D;

class Island : public QObject
{
    Q_OBJECT
    friend class FieldInfo;
    friend class RoutingParameters;

public:
    struct FieldData
    {
        FieldData();
        inline bool hasRoad() const { Q_ASSERT((roadType != 0) == (roadNetworkID != -1)); return roadType != 0; }
        inline bool hasBuilding() const { return building != 0; }
        inline int hasOneOfLicenses(int licenses) const { return licenses & buildingLicenses; }
        inline void grantLicenses(int licenses) { buildingLicenses |= licenses; }

        uint terrain : OAE_BITS_TERRAIN_TYPES;

        uint roadType : OAE_BITS_ROAD_TYPES;
        int roadNetworkID : 32;

        Building *building;

        PlayerID owner : OAE_BITS_PLAYER_IDS;
        int buildingLicenses : OAE_MAX_BUILDING_LICENSES;
    };

    enum BuildabilityStatus {
        Buildable,
        NotBuildableTerrain,
        NotBuildableNotOnIsland,
        NotBuildableCollision,
        NotBuildableLicense
    };

    explicit Island(World *world, QRect boundaries);
    ~Island();

    // TEST
    int _finalNetwork(int networkID) const { return finalNetwork(networkID); }

public slots:
    World *world() const;
    Game *game() const;

    QList<Town*> towns() const;
    Town* townOfThisPlayer() const;

    QRect boundaries() const;
    inline bool checkBounds(int x, int y) const { return x >= 0 && x < m_w && y >= 0 && y < m_h; }
    inline bool checkBounds(QPoint p) const { return checkBounds(p.x(), p.y()); }

    inline const FieldData &fieldAt(int x, int y) const { Q_ASSERT(checkBounds(x, y)); return m_fields[index(x, y)]; }
    inline const FieldData &fieldAt(QPoint p) const { Q_ASSERT(checkBounds(p)); return m_fields[index(p.x(), p.y())]; }

    BuildabilityStatus checkBuildability(PlayerID player, BuildingPrototype *proto, QPoint pos, Orientation orientation) const;
    BuildabilityStatus checkBuildability(PlayerID player, RoadPrototype *proto, QPoint pos) const;
    QList<QPoint> shortestBuildableRoadPath(PlayerID player, RoadPrototype *proto, QPoint source, QPoint target) const;

signals:
    void reportNewBuilding(Building *building);
    void reportRoadChanged(Island *island, QPoint pos, uint road);

private:
libprivate:
    uint road(QPoint pos) const;
    void setRoad(QPoint pos, uint road);

    void registerNewTown(Town *town);
    void registerDeleteTown(Town *town);

    void dumpRoadNetworks() const;

    int roadNetwork(QPoint pos);
    QSet<Building*> buildingsInRoadNetwork(int network);
    bool roadNetworkStillExists(int network); // If a class holds cached data about a network, it can delete this if a road network doesn't still exist.

private:
    struct RoadNetworkData
    {
        friend class GameView;

        RoadNetworkData();
        RoadNetworkData(QPoint representativeField);
        bool hasSuccessor() const { return successorID != -1; }
        void setSuccessor(int succ) { Q_ASSERT(!hasSuccessor()); successorID = succ; buildings.clear(); }

        QPoint representativeField;
        int successorID;
        QSet<Building*> buildings;
    };

    inline int index(int x, int y) const { Q_ASSERT(checkBounds(x, y)); return x + y * m_h; }
    inline FieldData &field(int x, int y) { Q_ASSERT(checkBounds(x, y)); return m_fields[index(x, y)]; }
    inline FieldData &field(QPoint p) { Q_ASSERT(checkBounds(p)); return m_fields[index(p.x(), p.y())]; }

    /** Follows the "linked list" of networkIDs until we find a network without a successor. */
    int finalNetwork(int networkID) const;

    /** Assign and place a new networkID on the given field. The new networkID is returned. */
    int makeNetwork(QPoint pos);

    /** Look for buildings in the given network, update the set of buildings for this network and set the network
     *  ID on the fields to this ID asserting that they are predecessors of it. */
    void traverseAndSetNewRoadNetwork(int networkID);
    void traverseAndSetNewRoadNetworkDFS(int networkID, QPoint comingFrom, QPoint current,
                                         SparseBitArray2D &visited, QSet<Building *> &buildings);

private slots:
    void onNewBuilding(Building *building);
    void onDeleteBuilding(Building *building);

//public:
private:
    QList<Town*> m_towns;

    QRect m_boundaries;
    int m_w, m_h, m_s;
    FieldData *m_fields;
    int m_nextRoadNetworkID;
    QList<RoadNetworkData> m_roadNetworks;
};

OAE_END_NAMESPACE

#endif // ISLAND_H
