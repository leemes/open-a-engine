#ifndef IDS_H
#define IDS_H

#include "global.h"
#include <QSize>

OAE_BEGIN_NAMESPACE

// ===== IDS =====

// Players
typedef qint8 PlayerID;
static const PlayerID NO_PLAYER = -1;

// Goods
typedef quint16 GoodID;



// ===== ENUMS =====

typedef quint8 Orientation;
static const Orientation DEFAULT_ORIENTATION = 0;

inline QSize orientedSize(QSize originalSize, Orientation orientation) {
    if(orientation % 2)
        return QSize(originalSize.height(), originalSize.width());
    else
        return originalSize;
}

enum RoutingStrategy {
    ShortestPath,
    FastestPath
};


// ===== TIME =====

typedef int Time;
typedef double TimeF;
static const int msPerTimeUnit = 100;

inline TimeF timeFromMilliSeconds(double ms) {
    return ms / msPerTimeUnit;
}
inline TimeF timeFromSeconds(double sec) {
    return timeFromMilliSeconds(sec * 1000.0);
}
inline double timeToMilliSeconds(TimeF t) {
    return t * msPerTimeUnit;
}
inline double timeToSeconds(TimeF t) {
    return timeToMilliSeconds(t) / 1000.0;
}

OAE_END_NAMESPACE

#endif // IDS_H
