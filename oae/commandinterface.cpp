#include "commandinterface.h"
#include "internal/commands/command_debug.h"
#include "internal/commands/command_addgoodsisland.h"
#include "internal/commands/command_createbuilding.h"
#include "internal/commands/command_createroad.h"
#include "internal/commands/command_deleteroad.h"


CommandInterface::CommandInterface(Game *game) :
    m_game(game)
{
}

void CommandInterface::setGame(Game *game)
{
    m_game = game;
}



// All command wrappers are implemented the same way. So we use a macro for the implementations.
// The macro parameter has to contain the command name, which is used as the class name suffix as well as the arguments in parentheses.
#define IMPL(CommandCall) \
    return QSharedPointer<Command>(new Command(m_game, new Command_##CommandCall))


QSharedPointer<Command> CommandInterface::debug(Town *town) {
    IMPL(Debug(town));
}
QSharedPointer<Command> CommandInterface::addGoodsIsland(Town *town, LocalGoods goods){
    IMPL(AddGoodsIsland(town, goods));
}
QSharedPointer<Command> CommandInterface::createBuilding(Town *town, uint protoID, QPoint position, Orientation orientation) {
    IMPL(CreateBuilding(town, protoID, position, orientation));
}
QSharedPointer<Command> CommandInterface::createRoad(Town *town, uint protoID, QList<QPoint> fields) {
    IMPL(CreateRoad(town, protoID, fields));
}
QSharedPointer<Command> CommandInterface::deleteRoad(Town *town, QList<QPoint> fields) {
    IMPL(DeleteRoad(town, fields));
}
