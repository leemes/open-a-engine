#ifndef TRANSPORTATIONPROTOTYPE_H
#define TRANSPORTATIONPROTOTYPE_H

#include "../global.h"
#include "../namespace.h"
#include <QString>

class QPoint;

OAE_BEGIN_NAMESPACE

class RoutingParameters;
class RoutingNeighbor;

class TransportationPrototype
{
    TransportationPrototype();
    friend class TransportationLibrary;
    friend class QVector<TransportationPrototype>;

    static uint nextID;

public:
    typedef QList<RoutingNeighbor> (*RoutingConditionFunc)(RoutingParameters*);
    /*
    enum Class {
        ClassInvalid
    };
    */

    uint id;

    QString name;
    //Class transportationClass;

    QString propertyName;
    RoutingConditionFunc routingCondition;
    RoutingStrategy routingStrategy;
    float baseSpeed;
    int capacity;
    struct {
        Time startHome;
        Time stopClient;
        Time startClient;
        Time stopHome;
    } delays;
};

OAE_END_NAMESPACE

#endif // TRANSPORTATIONPROTOTYPE_H
