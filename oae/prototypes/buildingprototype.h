#ifndef BUILDINGPROTOTYPE_H
#define BUILDINGPROTOTYPE_H

#include "../global.h"
#include "../namespace.h"
#include "../goods.h"
#include <QString>
#include <QImage>
#include <QSize>
#include <QMap>

OAE_BEGIN_NAMESPACE

class Island;

class BuildingPrototype
{
    BuildingPrototype();
    friend class BuildingLibrary;
    friend class QVector<BuildingPrototype>;

    static uint nextID;

public:
    enum Type {
        TypeInvalid,
        TypeLand,
        TypeCoast,
        TypeWater,
        TypeRiver
    };

    enum Class {
        ClassInvalid,
        ClassDecorative,
        ClassTrade,
        ClassNeed,
        ClassResidence,
        ClassProduction,
        ClassMilitary
    };

    struct Upgrade {
        BuildingPrototype *targetPrototype;
        Goods upgradeCosts;
        // TODO: upgrade conditions
    };

    uint id;

    // mandatory:
    QString name;
    Type buildingType;
    Class buildingClass;
    QImage icon;
    QSize size;

    // optional:
    bool canBeBuiltDirectly;
    Goods constructionCosts;
    float constructionCostsDivisor;
    // TODO: construction conditions
    int maintenanceActive;
    int maintenanceInactive;
    int storageCapacity;
    int strongboxSlots;
    int tradeSlots;
    GoodID productionGood;
    Goods productionInputs;
    int productionInputCapacity;
    int productionCapacity;
    Time productionTime;
    float influenceRadius;

    // building license:
    int requireLicenses; // bitfield -- at least one of them required
    QList<int> requireLicensesList() const; // the same, but as a list instead of a bitfield
    bool requireLicenseOverlap; // true: only overlap needed; false: target needs to be covered
    QMap<int,int> grantLicenses; // key: license, value: radius

    QMap<uint,int> transportationUnits; // transportationProtoID => count

    QVector<Upgrade> upgrades;

    // geometry:
    QList<QPoint> coveredFields(QPoint origin, Orientation orientation) const;
    int coveredFieldsCount() const;
    QList<QPoint> possibleStreetConnections(QPoint origin, Orientation orientation, Island *island) const;
    QList<QPoint> areaForRadius(QPoint origin, Orientation orientation, int radius, const Island *island) const;
};

OAE_END_NAMESPACE

#endif // BUILDINGPROTOTYPE_H
