#include "buildingprototype.h"
#include "../island.h"

uint BuildingPrototype::nextID = 1;

BuildingPrototype::BuildingPrototype() :
    id(nextID++),
    canBeBuiltDirectly(true),
    maintenanceActive(0),
    maintenanceInactive(0),
    storageCapacity(0),
    strongboxSlots(0),
    tradeSlots(0),
    productionInputCapacity(0),
    productionCapacity(0),
    productionTime(0),
    influenceRadius(0)
{
}

QList<int> BuildingPrototype::requireLicensesList() const
{
    int bits = requireLicenses;
    QList<int> list;
    for(int bit = 1; bits != 0; bit <<= 1) {
        if(bits & bit) {
            list << bit;  // insert license into the list
            bits &= ~bit; // remove bit from bitfield
        }
    }
    return list;
}

QList<QPoint> BuildingPrototype::coveredFields(QPoint origin, Orientation orientation) const
{
    QSize oSize = orientedSize(size, orientation);

    QList<QPoint> points;
    for(int y = origin.y(); y < origin.y() + oSize.height(); ++y)
        for(int x = origin.x(); x < origin.x() + oSize.width(); ++x)
            points << QPoint(x, y);

    return points;
}

int BuildingPrototype::coveredFieldsCount() const
{
    return size.width() * size.height();
}

QList<QPoint> BuildingPrototype::possibleStreetConnections(QPoint origin, Orientation orientation, Island *island) const
{
    QSize oSize = orientedSize(size, orientation);
    int above = origin.y() - 1;
    int below = origin.y() + oSize.height();
    int toTheLeft = origin.x() - 1;
    int toTheRight = origin.x() + oSize.width();

    QList<QPoint> points;
    for(int x = origin.x(); x < origin.x() + oSize.width(); ++x) {
        if(island->checkBounds(x, above))
            points << QPoint(x, above);
        if(island->checkBounds(x, below))
            points << QPoint(x, below);
    }
    for(int y = origin.y(); y < origin.y() + oSize.height(); ++y) {
        if(island->checkBounds(toTheLeft, y))
            points << QPoint(toTheLeft, y);
        if(island->checkBounds(toTheRight, y))
            points << QPoint(toTheRight, y);
    }

    return points;
}

QList<QPoint> BuildingPrototype::areaForRadius(QPoint origin, Orientation orientation, int radius, const Island *island) const
{
    qreal r = radius;
    qreal rSquare = r * r;
    QRect boundaries(origin, orientedSize(size, orientation));
    if(boundaries.width() % 2 && boundaries.height() % 2)
        r -= .5;

    QList<QPoint> points;
    QPointF center((boundaries.left() + boundaries.right()) * 0.5,
                   (boundaries.top() + boundaries.bottom()) * 0.5);

    // start with the center point, maybe this is between a field, so take 2 (or even 4) fields as "the center":
    QRect areaRect((center - QPointF(.5, .5)).toPoint(), center.toPoint());
    areaRect.adjust(-radius, -radius, radius, radius);

    for(int y = areaRect.top(); y <= areaRect.bottom(); ++y)
    {
        for(int x = areaRect.left(); x <= areaRect.right(); ++x)
        {
            if(island->checkBounds(x, y))
            {
                qreal dx = x - center.x();
                qreal dy = y - center.y();
                if(dx * dx + dy * dy <= rSquare)
                    points << QPoint(x, y);
            }
        }
    }
    return points;
}
