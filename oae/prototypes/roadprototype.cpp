#include "roadprototype.h"

uint RoadPrototype::nextID = 1;

RoadPrototype::RoadPrototype() :
    id(nextID++),
    speedFactor(1.0)
{
}
