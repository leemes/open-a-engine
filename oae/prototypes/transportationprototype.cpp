#include "transportationprototype.h"

uint TransportationPrototype::nextID = 1;

TransportationPrototype::TransportationPrototype() :
    id(nextID++),
    routingStrategy(ShortestPath),
    baseSpeed(1.0),
    capacity(1)
{
    delays.startHome = 0;
    delays.stopClient = 0;
    delays.startClient = 0;
    delays.stopHome = 0;
}
