#ifndef GOODPROTOTYPE_H
#define GOODPROTOTYPE_H

#include "../global.h"
#include <QImage>
#include <QMap>

OAE_BEGIN_NAMESPACE

class GoodPrototype
{
    GoodPrototype();
    friend class GoodLibrary;
    friend class QVector<GoodPrototype>;

    static uint nextID;

public:
    enum Class {
        ClassInvalid,
        ClassGlobal,
        ClassLocal
    };

    uint id;

    QString name;
    Class goodClass;
    QImage icon;

    int limit;

    // provided as a shorthand to check a good for its class without the need for the prototype - set by GoodLibrary
    static uint firstLocalGoodID;
    // provided as a shorthand to coins
    static uint coinsID;
};

OAE_END_NAMESPACE

#endif // GOODPROTOTYPE_H
