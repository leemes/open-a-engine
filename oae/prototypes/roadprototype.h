#ifndef ROADPROTOTYPE_H
#define ROADPROTOTYPE_H

#include "../global.h"
#include "../namespace.h"
#include "../goods.h"
#include <QImage>
#include <QMap>

OAE_BEGIN_NAMESPACE

class RoadPrototype
{
    RoadPrototype();
    friend class RoadLibrary;
    friend class QVector<RoadPrototype>;

    static uint nextID;

public:
    enum Class {
        ClassInvalid,
        ClassPath,
        ClassPlace
    };

    uint id;

    QString name;
    Class roadClass;
    QImage icon;

    Goods constructionCosts;
    float constructionCostsDivisor;
    float speedFactor;
    int requireLicenses;
};

OAE_END_NAMESPACE

#endif // ROADPROTOTYPE_H
