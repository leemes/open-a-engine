#ifndef MANAGER_H
#define MANAGER_H

#include "global.h"
#include "namespace.h"
#include "commandinterface.h"
#include <QObject>
#include <QQueue>
#include <QList>
#include <QElapsedTimer>

class QTimer;

OAE_BEGIN_NAMESPACE

class Game;
class Scheduler;
class Command;

class Manager : public QObject
{
    Q_OBJECT
public:
    explicit Manager(Game *game);

    Game *game() const;
    Scheduler *scheduler() const;

    Time time() const;
    TimeF timeF() const;

public slots:
    void start();
    void pause();
    bool running() const;
    void setDefaultSpeed();
    void setSpeed(float speed);
    float speed() const;

    void enqueueCommand(QSharedPointer<Command> cmd);

private:
libprivate:
    Q_SLOT void iterate();
    
private:
    static const Time initialDelay = 1;

    Scheduler *m_scheduler;
    QTimer *m_timer;

    Time m_time;
    QElapsedTimer m_elapsedAfterLastTick;
    Time m_delay;
    float m_speed;
    QQueue<QList<QSharedPointer<Command> > > m_queue; // queue's head corresponds to the next time step
};

OAE_END_NAMESPACE

#endif // MANAGER_H
