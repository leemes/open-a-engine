#include "goods.h"
#include "prototypes/goodprototype.h"
#include <QVariant>
#include <cmath>


LocalGoods LocalGoods::operator +(const LocalGoods &other) const
{
    LocalGoods result = *this;
    foreachkv(GoodID good, int amount, other)
        result[good] += amount;
    return result;
}



Goods::Goods(const QMap<GoodID, int> &goods)
{
    foreachkv(GoodID good, int amount, goods)
        (*this)[good] = amount;
}

const GlobalGoods &Goods::globalGoods() const
{
    return m_global;
}

const LocalGoods &Goods::localGoods() const
{
    return m_local;
}

int Goods::operator [](GoodID good) const
{
    if(good < GoodPrototype::firstLocalGoodID)
        return m_global[good];
    else
        return m_local[good];
}

int &Goods::operator [](GoodID good)
{
    if(good < GoodPrototype::firstLocalGoodID)
        return m_global[good];
    else
        return m_local[good];
}

Goods Goods::operator*(double factor) const
{
    Goods copy;
    foreach(GoodID good, m_local.keys())
        copy.m_local[good] = floor(m_local[good] * factor);
    foreach(GoodID good, m_global.keys())
        copy.m_global[good] = floor(m_global[good] * factor);
    return copy;
}

Goods &Goods::operator*=(double factor)
{
    foreach(GoodID good, m_local.keys())
        m_local[good] = floor(m_local[good] * factor);
    foreach(GoodID good, m_global.keys())
        m_global[good] = floor(m_global[good] * factor);
    return *this;
}

Goods Goods::operator/(double divisor) const
{
    Goods copy;
    foreach(GoodID good, m_local.keys())
        copy.m_local[good] = floor(m_local[good] / divisor);
    foreach(GoodID good, m_global.keys())
        copy.m_global[good] = floor(m_global[good] / divisor);
    return copy;
}

Goods &Goods::operator/=(double divisor)
{
    foreach(GoodID good, m_local.keys())
        m_local[good] = floor(m_local[good] / divisor);
    foreach(GoodID good, m_global.keys())
        m_global[good] = floor(m_global[good] / divisor);
    return *this;
}

Goods &Goods::divideRoundedUp(double divisor)
{
    foreach(GoodID good, m_local.keys())
        m_local[good] = ceil(m_local[good] / divisor);
    foreach(GoodID good, m_global.keys())
        m_global[good] = ceil(m_global[good] / divisor);
    return *this;
}

QVariantMap Goods::toVariantMap() const
{
    QVariantMap map;
    foreach(GoodID good, m_local.keys())
        map[QString::number(good)] = m_local[good];
    foreach(GoodID good, m_global.keys())
        map[QString::number(good)] = m_global[good];
    return map;
}
