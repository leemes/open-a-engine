#ifndef COMMAND_H
#define COMMAND_H

#include "global.h"
#include <QVariantList>
#include "namespace.h"
#include "goods.h"

class QObject;
namespace QtSharedPointer {
template<class T, class D> class CustomDeleter;
class NormalDeleter;
}


OAE_BEGIN_NAMESPACE

class Game;
class Player;
class Island;
class Town;
class AbstractCommand;

/**
 * @brief Wrapper of AbstractCommand instances for use in the API.
 * When an instance of this class is created, it doesn't have a parent QObject.
 * The CommandInterface however returns a QSharedPointer instead of a raw pointer,
 * so it gets deleted by the QSharedPointer.
 */
class Command : public QObject
{
    Q_OBJECT
    Q_ENUMS(Error)

    // Only the manager (through its subclass CommandInterface) can create new instances of this class.
    friend class CommandInterface;
    // Only the QSharedPointer can delete the instance created by the interface
    friend class QtSharedPointer::CustomDeleter<Command, QtSharedPointer::NormalDeleter>;
    // The manager needs to modify m_time and call execute() on this class.
    friend class Manager;

public:
    enum Error {
        NoError,
        NotEnoughLocalResources,
        NotEnoughGlobalResources,
        NotBuildableNotOnIsland,
        NotBuildableTerrain,
        NotBuildableCollision,
        NotBuildableLicense
    };

    Game *game() const;
    Time time() const;

    Error check() const;

signals:
    void done(Error error);

private:
    explicit Command(Game *game, AbstractCommand *command);
    ~Command();

    // Called by the friend Manager::iterate()
    void execute();

private:
    Game *m_game;
    Time m_time;
    AbstractCommand *m_command;
};

OAE_END_NAMESPACE

#endif // COMMAND_H
