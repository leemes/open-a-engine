#ifndef TRANSPORTATION_H
#define TRANSPORTATION_H

#include "global.h"
#include <QObject>
#include <QPoint>
#include "goods.h"
#include "path.h"
#include "internal/routing/routingconditionfunctions.h"
#include "internal/scheduled.h"

OAE_BEGIN_NAMESPACE

class Building;
class TransportationPrototype;

/**
 * @brief The Transportation class
 *
 *
 * Location: /  State: /  Comment:
 *
 * Home         Idle    } The transportation unit is in its home building
 * =====================  Got the command from the home building that it should fetch or deliver some goods. The goods to be delivered are taken from the home building
 * HomeToClient Start   } The transportation unit waits a given period of time, located at the source field
 *             ---------  Start moving along the path (progress = 0.0)
 *              Run     } Moving along the path. The actual position can be interpolated using the progress, which is between 0.0 and 1.0
 *             ---------  Stop moving along the path (progress = 1.0)
 *              Stop    } The transportation unit waits a given period of time, located at the target field
 * =====================  The goods to be delivered are put in the client building; the goods to be fetched are taken.
 * ClientToHome Start    -.
 *             ---------  |
 *              Run        >  Same as above, but source = client and target = home.
 *             ---------  |
 *              Stop     -'
 * =====================  The goods to be fetched are put in the home building
 * Home         Idle    } The transportation unit is in its home building
 */
class Transportation : public QObject, private Scheduled
{
    Q_OBJECT
    friend class Building;
    friend class Command_Debug;

public:
    explicit Transportation(Building *building, TransportationPrototype *prototype);

    enum State {
        StateIdle,  //!< The transportation unit is in the home building
        StateStart, //!< Starts to visit its target building (its position still being at the source field)
        StateRun,   //!< Is on its way to the target building (the progress can be used to interpolate on the path)
        StateStop   //!< Arrived at the target building but didn't deliver the goods yet (its position being at the target field)
    };

    enum Location {
        LocationHome,          //!< The transportation unit is in its home building
        LocationHomeToClient,  //!< On the way there (source: home, target: client)
        LocationClientToHome   //!< On the way back (source: home, target: client)
    };


public slots:
    // Prototype:
    uint prototypeID() const;
    RoutingConditionFunc routingCondition() const;
    RoutingStrategy routingStrategy() const;
    qreal baseSpeed() const;
    int capacity() const;

    // Buildings:
    Building *home() const; //!< never changes
    Building *client() const; //!< the client building if during transport, 0 if idle

    // Current state:
    Location location() const;
    State state() const;
    Time lastStateChange() const;

    // Current state - as booleans:
    bool isIdle() const; //!< = true iif state = StateIdle which equals the condition location = LocationHome
    bool isBusy() const; //!< = false iif isAtHome

    // More details about the current state:
    Building *sourceBuilding() const; //!< = home if location = LocationHomeToClient, client if location = LocationClientToHome, 0 otherwise
    Building *targetBuilding() const; //!< = client if location = LocationHomeToClient, home if location = LocationClientToHome, 0 otherwise
    QPoint sourceField() const; //!< The road connection field at the sourceBuilding (besides the building)
    QPoint targetField() const; //!< The road connection field at the targetBuilding (besides the building)
    const Path & path() const; //!< The path in which the real position can be interpolated using progress()
    qreal progress() const; //!< = 0.0 if state = StateIdle or StatePre or StateStart, between 0.0 and 1.0 if state = StateRun, 1.0 if state = StateStop or StatePost

    LocalGoods goods() const;

private:
libprivate:
    void startJob(Building *client, Path path, LocalGoods deliver, LocalGoods collect);

private:
    void schedule();
    void deleteNow();

    void actionStartDelivery(const LocalGoods & goods);
    void actionStopDelivery();
    void actionStartCollection();
    void actionStopCollection();
    void actionFinished();
    
private:
    TransportationPrototype *m_proto;
    LocalGoods m_goods; // the goods currently being transported
    LocalGoods m_collect; // the goods to be collected at the client building
    Building *m_client; // (home is the parent QObject)
    State m_state;
    Location m_location;
    Path m_path;
    Time m_lastStateChange;
};

OAE_END_NAMESPACE

#endif // TRANSPORTATION_H
