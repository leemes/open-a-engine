#ifndef TOWN_H
#define TOWN_H

#include "global.h"
#include "namespace.h"
#include "goods.h"
#include "internal/scheduled.h"
#include <QObject>
#include <QMap>
#include <QPoint>

OAE_BEGIN_NAMESPACE

class Player;
class Island;
class Building;
class World;
class Game;

class Town : public QObject, public Scheduled
{
    Q_OBJECT

public:
    explicit Town(Player *player, Island *island, QString name);
    ~Town();

public slots:
    Player *player() const;
    Island *island() const;
    World *world() const;
    Game *game() const;
    QList<Building*> buildings() const;

    QString name() const;

    // goods storage
    int totalStorageCapacity() const;
    const LocalGoods & currentStorage() const;
    int currentStorage(GoodID good) const;
    bool checkGoodsInStorage(const LocalGoods & goods) const;
    bool checkGoodsInStorage(GoodID good, int amount) const;

    // information about reservations
    int reservedIncoming(GoodID good);
    int reservedOutgoing(GoodID good);

signals:
    void reportNewBuilding(Building *building);
    void reportDeleteBuilding(Building *building);

private:
libprivate:
    Building *newBuilding(QPoint position, Orientation orientation, uint protoID);
    void deleteBuilding(Building *building);

    // alter goods storage without reservation
    void addGoodsToStorage(const LocalGoods & goods);
    void addGoodsToStorage(GoodID good, int amount);
    bool takeGoodsFromStorage(const LocalGoods & goods);
    bool takeGoodsFromStorage(GoodID good, int amount);

    // reservation of incoming goods
    int currentStorageIncludingReservedIncoming(GoodID good) const;
    void reserveIncoming(GoodID good, int amount);
    void addReservedIncoming(GoodID good, int amount); // goods which don't fit get lost

    // reservation of outgoing goods
    int currentStorageExcludingReservedOutgoing(GoodID good) const;
    void reserveOutgoing(GoodID good, int amount);
    int takeReservedOutgoing(GoodID good, int amount);

private:
    void schedule();
    void deleteNow();

    void updateTotalStorageCapacity();

private:
    Island *m_island;

    QList<Building*> m_buildings;

    QString m_name;
    int m_totalStorageCapacity;
    LocalGoods m_storage;
    LocalGoods m_reservedIncoming;
    LocalGoods m_reservedOutgoing;
};

OAE_END_NAMESPACE

#endif // TOWN_H
