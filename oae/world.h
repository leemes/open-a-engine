#ifndef WORLD_H
#define WORLD_H

#include "global.h"
#include <QObject>
#include <QSize>
#include <QRect>
#include <QList>

OAE_BEGIN_NAMESPACE

class Game;
class Island;
class Town;
class Building;

class World : public QObject
{
    Q_OBJECT

public:
    explicit World(Game *game, QSize size);

public slots:
    Game *game() const;

    QSize size() const;
    int width() const;
    int height() const;

    Island *newIsland(QRect boundary);

    QList<Island*> islands() const;
    Island *islandAt(QPoint pos) const;

signals:
    void reportNewBuilding(Building *building);
    void reportRoadChanged(Island *island, QPoint pos, uint road);
    
private:
    QList<Island*> m_islands;

    QSize m_size;
};

OAE_END_NAMESPACE

#endif // WORLD_H
