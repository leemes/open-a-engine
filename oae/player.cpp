#include "player.h"
#include "game.h"
#include "town.h"
#include "island.h"
#include "libraries/goodlibrary.h"
#include "prototypes/goodprototype.h"


Player::Player(Game *game, PlayerID id) :
    QObject(game),
    m_id(id)
{
}

PlayerID Player::id() const
{
    return m_id;
}

Town *Player::newTown(Island *island, QString name)
{
    Town *town = new Town(this, island, name);
    m_towns << town;
    return town;
}

void Player::addGlobalGoods(QMap<GoodID, int> goods)
{
    foreachkv(GoodID good, int amount, goods)
    {
        m_globalGoods[good] += amount;
        int limit = GoodLibrary::prototype(good)->limit;
        if(limit && m_globalGoods[good] > limit)
            m_globalGoods[good] = limit;
    }
}

bool Player::checkGlobalGoods(QMap<GoodID, int> goods) const
{
    foreachkv(GoodID good, int amount, goods)
        if(m_globalGoods[good] < amount)
            return false;
    return true;
}

bool Player::takeGlobalGoods(QMap<GoodID, int> goods)
{
    if(!checkGlobalGoods(goods))
        return false;
    foreachkv(GoodID good, int amount, goods)
        m_globalGoods[good] -= amount;
    return true;
}

int Player::currentCoins() const
{
    return m_globalGoods[GoodPrototype::coinsID];
}

void Player::addCoins(int amount)
{
    m_globalGoods[GoodPrototype::coinsID] += amount;
}

void Player::takeCoins(int amount)
{
    m_globalGoods[GoodPrototype::coinsID] -= amount;
}

const QMap<GoodID, int> & Player::currentGlobalGoods() const
{
    return m_globalGoods;
}

int Player::currentGlobalGood(GoodID good) const
{
    return m_globalGoods[good];
}
