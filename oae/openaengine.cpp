#include "openaengine.h"
#include "game.h"
#include "island.h"
#include "town.h"
#include "building.h"
#include "libraries/abstractlibrary.h"
#include "libraries/goodlibrary.h"
#include "libraries/transportationlibrary.h"
#include "libraries/buildinglibrary.h"
#include "libraries/roadlibrary.h"
#include <QDebug>


void dumpObjectsHelper(const QObject *obj, int indent = 0)
{
    {
        // Debug the QObject pointer
        QDebug dbg = qDebug() << "" << qPrintable(QString("  ").repeated(indent)) << obj;

        // Helper macro for the following conditional blocks
#define IFTYPE(TYPE, VARNAME) if(const TYPE *VARNAME = qobject_cast<const TYPE*>(obj))

        // Debug some additional attributes
        IFTYPE(Island, island) {
            dbg << "at" << island->boundaries();
        }
        IFTYPE(Town, town) {
            dbg << town->name() << "on" << town->island();
        }
        IFTYPE(Building, building) {
            dbg << building->typeName();
        }
    }

    foreach(const QObject *child, obj->children())
        dumpObjectsHelper(child, indent + 1);
}


OpenAEngine *OpenAEngine::staticInstance = 0;


OpenAEngine::OpenAEngine(QDir resourcesBaseDir, QObject *parent) :
    QObject(parent),
    m_resBaseDir(resourcesBaseDir)
{
    if(staticInstance)
        qFatal("You can't have more than one instance of OpenAEngine.");
    staticInstance = this;

    QDir rulesBaseDir = m_resBaseDir.filePath("rules");
    AbstractLibrary::setRulesBaseDir(rulesBaseDir);

    QDir iconsBaseDir = m_resBaseDir.filePath("icons");
    AbstractLibrary::setIconsBaseDir(iconsBaseDir);
}

void OpenAEngine::initialize()
{
    registerMetaTypes();

    GoodLibrary::initialize();
    TransportationLibrary::initialize();
    BuildingLibrary::initialize();
    RoadLibrary::initialize();
}

Game *OpenAEngine::newGame(QSize size)
{
    Game *game = new Game(this, size);
    m_games << game;
    return game;
}

Game *OpenAEngine::newGame(int width, int height)
{
    return newGame(QSize(width, height));
}

QList<Game *> OpenAEngine::games() const
{
    return m_games;
}

QString OpenAEngine::goodName(GoodID goodID) const
{
    return GoodLibrary::prototype(goodID)->name;
}

void OpenAEngine::dumpObjects() const
{
    qDebug("OBJECTS:");
    dumpObjectsHelper(this);
}

void OpenAEngine::dumpLibraries() const
{
    qDebug("GOODS:");
    GoodLibrary::dumpContents();
    qDebug("BUILDINGS:");
    BuildingLibrary::dumpContents();
    qDebug("ROADS:");
    RoadLibrary::dumpContents();
    qDebug("TRANSPORTATION UNITS:");
    TransportationLibrary::dumpContents();
}

void OpenAEngine::registerNewError(Error e)
{
    emit error(e);
}

void OpenAEngine::registerMetaTypes()
{
    qRegisterMetaType<LocalGoods>();
    qRegisterMetaType<GlobalGoods>();
}
